package eu.ebrains.idm.configuration;

import eu.ebrains.idm.model.normalized.AbstractEntity;
import jakarta.persistence.PrePersist;
import jakarta.persistence.PreUpdate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import java.util.Date;

@Component
public class CustomEntityListener {

    private static final Logger logger = LoggerFactory.getLogger(CustomEntityListener.class);
    @PrePersist
    public void prePersist(Object entity) {
        if (entity instanceof AbstractEntity) {
            setUsernameCreation((AbstractEntity) entity);
            setDateCreation((AbstractEntity) entity);
        }
    }

    @PreUpdate
    public void preUpdate(Object entity) {
        if (entity instanceof AbstractEntity) {
            setUsernameUpdate((AbstractEntity) entity);
            setDateUpdate((AbstractEntity) entity);
        }
    }

    private void setDateCreation(AbstractEntity entity) {
        entity.setDateCreation(new Date());
    }

    private void setDateUpdate(AbstractEntity entity) {
        entity.setDateUpdate(new Date());
    }

    private void setUsernameCreation(AbstractEntity entity) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

        if (authentication != null && authentication.isAuthenticated()) {
            String username = authentication.getName();
            entity.setUserCreation(username);
        }
    }
    private void setUsernameUpdate(AbstractEntity entity) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

        if (authentication != null && authentication.isAuthenticated()) {
            String username = authentication.getName();
            entity.setUserUpdate(username);
        }
    }

}