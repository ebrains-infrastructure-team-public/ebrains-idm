package eu.ebrains.idm.configuration;

import eu.ebrains.idm.model.normalized.UserEntity;
import eu.ebrains.idm.repository.UserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.security.authentication.AbstractAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.oauth2.jwt.Jwt;
import org.springframework.security.oauth2.server.resource.authentication.JwtAuthenticationToken;
import org.springframework.security.oauth2.server.resource.authentication.JwtGrantedAuthoritiesConverter;

import java.util.Collection;
import java.util.Optional;
import java.util.stream.Stream;

import static java.util.Collections.emptySet;
import static java.util.stream.Collectors.toSet;

public class KeycloakJwtAuthenticationConverter implements Converter<Jwt, AbstractAuthenticationToken> {

    @Autowired
    private UserRepository userRepository;
    private static final Logger logger = LoggerFactory.getLogger(KeycloakJwtAuthenticationConverter.class);

    public KeycloakJwtAuthenticationConverter(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public AbstractAuthenticationToken convert(Jwt source) {
        JwtAuthenticationToken token =  new JwtAuthenticationToken(source, Stream.concat(new JwtGrantedAuthoritiesConverter().convert(source)
                        .stream(), extractEbrainseRoles(source).stream()).collect(toSet()));

        String username = token.getToken().getClaim("preferred_username");
        String sub = token.getToken().getClaim("sub");

        Optional<UserEntity> user = userRepository.findFirstByUsername(username);

        if(user.isEmpty()) {
            logger.info("User to create '{}' with sub '{}'", username, sub);
            UserEntity userEntity = new UserEntity(username,sub);
            userRepository.saveAndFlush(userEntity);
        }

        return token;
    }

    private Collection<? extends GrantedAuthority> extractEbrainseRoles(Jwt jwt) {
        // we can extend authrorites with ebrains specific stuff in the futur here
        return emptySet();
    }
}