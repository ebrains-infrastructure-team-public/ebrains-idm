package eu.ebrains.idm.utils.converter;

import eu.ebrains.idm.exception.ErrorMessage;
import eu.ebrains.idm.utils.enums.GroupType;
import jakarta.persistence.AttributeConverter;
import jakarta.persistence.Converter;

@Converter
public class GroupTypeConverter implements AttributeConverter<GroupType, String> {

    @Override
    public String convertToDatabaseColumn(GroupType type) {
        return type.getKey();
    }

    @Override
    public GroupType convertToEntityAttribute(String key) {
        for (GroupType type : GroupType.values()) {
            if (type.getKey().equals(key)) {
                return type;
            }
        }
        throw new IllegalArgumentException(String.format(ErrorMessage.UNKNOW_DATABASE_VALUE_ERROR_MSG, key));
    }
}