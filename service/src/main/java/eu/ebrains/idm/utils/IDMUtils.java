package eu.ebrains.idm.utils;

import eu.ebrains.idm.exception.ErrorMessage;
import eu.ebrains.idm.exception.LoginException;
import eu.ebrains.idm.utils.enums.Role;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

import java.util.regex.Pattern;

public class IDMUtils {

    private final static Pattern UUID_REGEX_PATTERN =
            Pattern.compile("^[{]?[0-9a-fA-F]{8}-([0-9a-fA-F]{4}-){3}[0-9a-fA-F]{12}[}]?$");

    public static boolean checkTeamRole(String role) {
        return role.equalsIgnoreCase(Role.ADMINISTRATOR.getKey())
                || role.equalsIgnoreCase(Role.EDITOR.getKey())
                || role.equalsIgnoreCase(Role.VIEWER.getKey());
    }

    public static boolean checkGroupRole(String role) {
        return role.equalsIgnoreCase(Role.ADMINISTRATOR.getKey())
                || role.equalsIgnoreCase(Role.MEMBER.getKey());
    }

    public static Boolean isValidTeamRoleChange(Role existingRole, Role newRole) {
        // Must return true if it's possible to change role, false if not, null if it's the same role
        switch (existingRole) {
            case VIEWER:
                switch (newRole) {
                    case VIEWER:
                        return null;
                    default:
                        return true;
                }
            case EDITOR:
                switch (newRole) {
                    case EDITOR:
                        return null;
                    case VIEWER:
                        return false;
                    case ADMINISTRATOR:
                        return true;
                }
            case ADMINISTRATOR:
                switch (newRole) {
                    case ADMINISTRATOR:
                        return null;
                    default:
                        return false;
                }
            default:
                return false;
        }
    }

    public static Boolean isValidGroupRoleChange(Role existingRole, Role newRole) {
        // Must return true if it's possible to change role, false if not, null if it's the same role
        switch (existingRole) {
            case MEMBER:
                switch (newRole) {
                    case MEMBER:
                        return null;
                    default:
                        return true;
                }
            case ADMINISTRATOR:
                switch (newRole) {
                    case ADMINISTRATOR:
                        return null;
                    default:
                        return false;
                }
            default:
                return false;
        }
    }

    public static boolean isValidUUID(String uuid) {
        if (uuid == null || "".equals(uuid)) {
            return false;
        }

        return UUID_REGEX_PATTERN.matcher(uuid).matches();
    }

    public static void checkUserLogged() throws LoginException {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication == null || !authentication.isAuthenticated() || authentication instanceof AnonymousAuthenticationToken) {
            throw new LoginException(ErrorMessage.LOGIN_REQUIRED_MSG);
        }
    }

    public static String getUserLogged() throws LoginException {
        checkUserLogged();
        return SecurityContextHolder.getContext().getAuthentication().getName();
    }

}
