package eu.ebrains.idm.utils.enums;

import eu.ebrains.idm.exception.EnumNotFoundException;
import eu.ebrains.idm.exception.ErrorMessage;

public enum GroupType {
    GROUP("group", "group"),
    UNIT("unit", "unit");

    private final String key;
    private final String description;

    GroupType(String key, String description) {
        this.key = key;
        this.description = description;
    }

    public String getKey() {
        return key;
    }

    public String getDescription() {
        return description;
    }

    public static Role fromKey(String key) {
        for (Role role : Role.values()) {
            if (role.getKey().equals(key)) {
                return role;
            }
        }
        throw new EnumNotFoundException(String.format(ErrorMessage.GROUP_TYPE_NOT_FOUND_ERROR_MSG, key));
    }

}
