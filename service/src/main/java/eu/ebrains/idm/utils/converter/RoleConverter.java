package eu.ebrains.idm.utils.converter;

import eu.ebrains.idm.exception.ErrorMessage;
import eu.ebrains.idm.utils.enums.Role;

import jakarta.persistence.*;

@Converter
public class RoleConverter implements AttributeConverter<Role, String> {

    @Override
    public String convertToDatabaseColumn(Role role) {
        return role.getKey();
    }

    @Override
    public Role convertToEntityAttribute(String key) {
        for (Role role : Role.values()) {
            if (role.getKey().equals(key)) {
                return role;
            }
        }
        throw new IllegalArgumentException(String.format(ErrorMessage.UNKNOW_DATABASE_VALUE_ERROR_MSG, key));
    }
}