package eu.ebrains.idm.mapper;

import eu.ebrains.idm.model.dto.GroupRole;
import eu.ebrains.idm.model.normalized.GroupRoleEntity;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface GroupRoleMapper {
    GroupRole groupRoleEntityToGroupRole(GroupRoleEntity groupRoleEntity);
    GroupRoleEntity groupRoleToGroupRoleEntity(GroupRole groupRole);
    List<GroupRole> groupRoleEntitiesToGroupRoles(List<GroupRoleEntity> groupRoleEntities);
    List<GroupRoleEntity> groupRolesToGroupRoleEntities(List<GroupRole> groupRoles);
}
