package eu.ebrains.idm.mapper;

import eu.ebrains.idm.model.dto.CollabUser;
import eu.ebrains.idm.model.dto.User;
import eu.ebrains.idm.model.normalized.UserEntity;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

import java.util.List;

@Mapper(componentModel = "spring")
public interface CollabUserMapper {
    @Mappings({
            @Mapping(source = "emailAddress", target = "email")
    })
    CollabUser userToCollabUser(User user);

    @Mappings({
            @Mapping(source = "email", target = "emailAddress")
    })
    User collabUserToUser(CollabUser collabUser);
}
