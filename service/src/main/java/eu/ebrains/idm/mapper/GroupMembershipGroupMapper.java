package eu.ebrains.idm.mapper;

import eu.ebrains.idm.model.dto.GroupMembershipGroup;
import eu.ebrains.idm.model.normalized.GroupMembershipGroupEntity;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface GroupMembershipGroupMapper {

    GroupMembershipGroup groupMembershipGroupEntityToGroupMembershipGroup(GroupMembershipGroupEntity groupMembershipGroupEntity);
    GroupMembershipGroupEntity groupMembershipGroupToGroupMembershipGroupEntity(GroupMembershipGroup groupMembershipGroup);
    List<GroupMembershipGroup> groupMembershipGroupEntitiesToGroupMembershipGroups(List<GroupMembershipGroupEntity> groupMembershipGroupEntities);
    List<GroupMembershipGroupEntity> groupMembershipGroupsToGroupMembershipGroupEntities(List<GroupMembershipGroup> groupMembershipGroups);
}
