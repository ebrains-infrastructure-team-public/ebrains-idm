package eu.ebrains.idm.mapper;

import eu.ebrains.idm.model.dto.Team;
import eu.ebrains.idm.model.normalized.TeamEntity;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

import java.util.List;

@Mapper(componentModel = "spring")
public interface TeamMapper {

    @Mapping(target = "teamMembershipGroups", ignore = true)
    @Mapping(target = "teamMembershipUsers", ignore = true)
    Team teamEntityToTeam(TeamEntity teamEntity);

    @Mapping(target = "teamMembershipGroups", ignore = true)
    @Mapping(target = "teamMembershipUsers", ignore = true)
    TeamEntity teamToTeamEntity(Team team);

    @Mapping(target = "teamMembershipGroups", ignore = true)
    @Mapping(target = "teamMembershipUsers", ignore = true)
    List<Team> teamEntitiesToTeams(List<TeamEntity> teamEntities);

    @Mapping(target = "teamMembershipGroups", ignore = true)
    @Mapping(target = "teamMembershipUsers", ignore = true)
    List<TeamEntity> teamsToTeamEntities(List<Team> teams);
}
