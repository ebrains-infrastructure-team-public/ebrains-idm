package eu.ebrains.idm.mapper;

import eu.ebrains.idm.model.dto.User;
import eu.ebrains.idm.model.normalized.UserEntity;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

import java.util.List;

@Mapper(componentModel = "spring")
public interface UserMapper {
    User userEntityToUser(UserEntity userEntity);
    UserEntity userToUserEntity(User user);
    List<User> userEntitiesToUsers(List<UserEntity> userEntities);
    List<UserEntity> usersToUserEntities(List<User> users);
}
