package eu.ebrains.idm.mapper;

import eu.ebrains.idm.model.dto.Team;
import eu.ebrains.idm.model.dto.TeamView;
import eu.ebrains.idm.model.normalized.TeamEntity;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface TeamViewMapper {

    TeamView teamEntityToTeamView(TeamEntity teamEntity);

    TeamEntity teamViewToTeamEntity(TeamView teamView);

    TeamView teamToTeamView(Team team);

    Team teamViewToTeam(TeamView teamView);

    List<TeamView> teamEntitiesToTeamViews(List<TeamEntity> teamEntities);

    List<TeamEntity> teamViewsToTeamEntities(List<TeamView> teamViews);

    List<TeamView> teamsToTeamViews(List<Team> teams);

    List<Team> teamViewsToTeams(List<TeamView> teamViews);
}
