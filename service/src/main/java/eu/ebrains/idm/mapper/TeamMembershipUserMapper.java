package eu.ebrains.idm.mapper;

import eu.ebrains.idm.model.dto.TeamMembershipUser;
import eu.ebrains.idm.model.normalized.TeamMembershipUserEntity;
import org.mapstruct.*;

import java.util.List;

@Mapper(componentModel = "spring")
public interface TeamMembershipUserMapper {
    @Named("teamMembershipUserEntityToTeamMembershipUser")
    @Mapping(target = "team", ignore = true)
    TeamMembershipUser teamMembershipUserEntityToTeamMembershipUser(TeamMembershipUserEntity teamMembershipUserEntity);
    TeamMembershipUserEntity teamMembershipUserToTeamMembershipUserEntity(TeamMembershipUser teamMembershipUser);
    @IterableMapping(qualifiedByName="teamMembershipUserEntityToTeamMembershipUser")
    List<TeamMembershipUser> teamMembershipUserEntitiesToTeamMembershipUsers(List<TeamMembershipUserEntity> teamMembershipUserEntities);
    List<TeamMembershipUserEntity> teamMembershipUsersToTeamMembershipUserEntities(List<TeamMembershipUser> teamMembershipUsers);
}
