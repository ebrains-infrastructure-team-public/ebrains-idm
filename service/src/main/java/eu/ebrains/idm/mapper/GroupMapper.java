package eu.ebrains.idm.mapper;

import eu.ebrains.idm.model.dto.Group;
import eu.ebrains.idm.model.dto.GroupRole;
import eu.ebrains.idm.model.dto.Unit;
import eu.ebrains.idm.model.normalized.GroupEntity;
import eu.ebrains.idm.utils.enums.GroupType;
import org.mapstruct.*;
import org.mapstruct.factory.Mappers;

import java.util.List;
import java.util.stream.Collectors;

@Mapper(componentModel = "spring")
public interface GroupMapper {

    @Named("groupEntityToGroup")
    @Mapping(target = "teamMembershipGroups", ignore = true)
    @Mapping(target = "groupMembershipGroups", ignore = true)
    Group groupEntityToGroup(GroupEntity groupEntity);
    GroupEntity groupToGroupEntity(Group group);
    @IterableMapping(qualifiedByName="groupEntityToGroup")
    List<Group> groupEntitiesToGroups(List<GroupEntity> groupEntities);
    List<GroupEntity> groupsToGroupEntities(List<Group> groups);

    Unit groupToUnit(Group group);
}
