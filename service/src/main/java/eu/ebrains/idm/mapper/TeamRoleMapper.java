package eu.ebrains.idm.mapper;

import eu.ebrains.idm.model.dto.TeamRole;
import eu.ebrains.idm.model.normalized.TeamRoleEntity;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface TeamRoleMapper {
    TeamRole teamRoleEntityToTeamRole(TeamRoleEntity teamRoleEntity);
    TeamRoleEntity teamRoleToTeamRoleEntity(TeamRole teamRole);
    List<TeamRole> teamRoleEntitiesToTeamRoles(List<TeamRoleEntity> teamRoleEntities);
    List<TeamRoleEntity> teamRolesToTeamRoleEntities(List<TeamRole> teamRoles);
}
