package eu.ebrains.idm.mapper;

import eu.ebrains.idm.model.dto.Group;
import eu.ebrains.idm.model.dto.TeamMembershipGroup;
import eu.ebrains.idm.model.normalized.GroupEntity;
import eu.ebrains.idm.model.normalized.TeamMembershipGroupEntity;
import org.mapstruct.IterableMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;

import java.util.List;

@Mapper(componentModel = "spring")
public interface TeamMembershipGroupMapper {
    @Named("teamMembershipGroupEntityToTeamMembershipGroup")
    @Mapping(target = "team", ignore = true)
    TeamMembershipGroup teamMembershipGroupEntityToTeamMembershipGroup(TeamMembershipGroupEntity teamMembershipGroupEntity);
    TeamMembershipGroupEntity teamMembershipGroupToTeamMembershipGroupEntity(TeamMembershipGroup teamMembershipGroup);
    @IterableMapping(qualifiedByName="teamMembershipGroupEntityToTeamMembershipGroup")
    List<TeamMembershipGroup> teamMembershipGroupEntitiesToTeamMembershipGroups(List<TeamMembershipGroupEntity> teamMembershipGroupEntities);
    List<TeamMembershipGroupEntity> teamMembershipGroupsToTeamMembershipGroupEntities(List<TeamMembershipGroup> teamMembershipGroups);
    @Mapping(target = "teamMembershipGroups", ignore = true)
    @Mapping(target = "groupMembershipGroups", ignore = true)
    GroupEntity groupToGroupEntity(Group group);

    @Mapping(target = "teamMembershipGroups", ignore = true)
    @Mapping(target = "groupMembershipGroups", ignore = true)
    Group groupToGroupEntity(GroupEntity groupEntity);
}
