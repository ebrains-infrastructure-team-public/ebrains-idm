package eu.ebrains.idm.mapper;

import eu.ebrains.idm.model.dto.GroupMembershipUser;
import eu.ebrains.idm.model.normalized.GroupMembershipUserEntity;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

import java.util.List;

@Mapper(componentModel = "spring")
public interface GroupMembershipUserMapper {
    GroupMembershipUser groupMembershipUserEntityToGroupMembershipUser(GroupMembershipUserEntity groupMembershipUserEntity);
    GroupMembershipUserEntity groupMembershipUserToGroupMembershipUserEntity(GroupMembershipUser groupMembershipUser);
    List<GroupMembershipUser> groupMembershipUserEntitiesToGroupMembershipUsers(List<GroupMembershipUserEntity> groupMembershipUserEntities);
    List<GroupMembershipUserEntity> groupMembershipUsersToGroupMembershipUserEntities(List<GroupMembershipUser> groupMembershipUsers);
}
