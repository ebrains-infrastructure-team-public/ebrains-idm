package eu.ebrains.idm.repository;

import eu.ebrains.idm.model.normalized.GroupMembershipUserEntity;
import eu.ebrains.idm.model.normalized.TeamMembershipUserEntity;
import eu.ebrains.idm.utils.enums.Role;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface GroupMembershipUserRepository extends JpaRepository<GroupMembershipUserEntity, String> {
    Optional<GroupMembershipUserEntity> findFirstByGroupNameAndUserUsername(String groupName, String username);

    List<GroupMembershipUserEntity> findAllByGroupNameAndGroupRoleRole(String groupName, Role role);
}
