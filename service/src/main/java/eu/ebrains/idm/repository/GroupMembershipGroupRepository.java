package eu.ebrains.idm.repository;

import eu.ebrains.idm.model.normalized.GroupMembershipGroupEntity;
import eu.ebrains.idm.model.normalized.TeamMembershipGroupEntity;
import eu.ebrains.idm.utils.enums.Role;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface GroupMembershipGroupRepository extends JpaRepository<GroupMembershipGroupEntity, String> {

    Optional<GroupMembershipGroupEntity> findFirstByParentGroupNameAndChildGroupName(String groupName, String childGroupName);
    List<GroupMembershipGroupEntity> findAllByParentGroupNameAndGroupRoleRole(String groupName, Role role);
}
