package eu.ebrains.idm.repository;

import eu.ebrains.idm.model.normalized.TeamRoleEntity;
import eu.ebrains.idm.utils.enums.Role;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TeamRoleRepository extends JpaRepository<TeamRoleEntity, Long> {

    TeamRoleEntity findFirstByRole(Role role);
}
