package eu.ebrains.idm.repository;

import eu.ebrains.idm.model.normalized.GroupRoleEntity;
import eu.ebrains.idm.utils.enums.Role;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface GroupRoleRepository extends JpaRepository<GroupRoleEntity, Long> {
    GroupRoleEntity findFirstByRole(Role role);
}
