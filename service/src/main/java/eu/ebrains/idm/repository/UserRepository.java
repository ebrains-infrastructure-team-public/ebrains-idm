package eu.ebrains.idm.repository;

import eu.ebrains.idm.model.normalized.UserEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<UserEntity, String> {
    Optional<UserEntity> findFirstByUsername(String username);
    @Query("SELECT u FROM UserEntity u WHERE u.username LIKE %:search% OR u.lastname LIKE %:search% OR u.firstname LIKE %:search% OR u.emailAddress LIKE %:search%")
    List<UserEntity> searchUsers(String search);
}
