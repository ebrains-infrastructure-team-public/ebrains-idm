package eu.ebrains.idm.repository;

import eu.ebrains.idm.model.normalized.GroupEntity;
import eu.ebrains.idm.model.normalized.TeamEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface GroupRepository extends JpaRepository<GroupEntity, String> {
    Optional<GroupEntity> findByNameIgnoreCase(String lowerCase);
}
