package eu.ebrains.idm.repository;

import eu.ebrains.idm.model.normalized.TeamMembershipUserEntity;
import eu.ebrains.idm.utils.enums.Role;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface TeamMembershipUserRepository extends JpaRepository<TeamMembershipUserEntity, String> {
    Optional<TeamMembershipUserEntity> findFirstByTeamIdAndUserId(String teamId, String userId);
    Optional<TeamMembershipUserEntity> findFirstByTeamNameAndUserUsername(String teamName, String username);
    List<TeamMembershipUserEntity> findAllByTeamNameAndTeamRoleRole(String teamId, Role role);
}
