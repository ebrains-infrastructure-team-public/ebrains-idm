package eu.ebrains.idm.repository;

import eu.ebrains.idm.model.normalized.TeamEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface TeamRepository extends JpaRepository<TeamEntity, String> {

    Optional<TeamEntity> findByNameIgnoreCase(String name);
}
