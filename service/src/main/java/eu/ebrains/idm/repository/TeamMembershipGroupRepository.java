package eu.ebrains.idm.repository;

import eu.ebrains.idm.model.normalized.TeamMembershipGroupEntity;
import eu.ebrains.idm.utils.enums.Role;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface TeamMembershipGroupRepository extends JpaRepository<TeamMembershipGroupEntity, String> {
    Optional<TeamMembershipGroupEntity> findFirstByTeamNameIgnoreCaseAndGroupId(String teamName, String groupId);
    Optional<TeamMembershipGroupEntity> findFirstByTeamNameIgnoreCaseAndGroupNameIgnoreCase(String teamName, String groupName);
    List<TeamMembershipGroupEntity> findAllByTeamNameAndTeamRoleRole(String teamId, Role role);
}
