package eu.ebrains.idm.model.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import eu.ebrains.idm.utils.enums.GroupType;
import jakarta.persistence.Column;

import java.util.List;

public class Group {
    @JsonIgnore
    private String id;
    private String name;
    private String title;
    private String description;
    private GroupType type;
    @JsonIgnore
    private List<TeamMembershipGroup> teamMembershipGroups;
    @JsonIgnore
    private List<GroupMembershipGroup> groupMembershipGroups;
    public Group() {
    }

    public Group(String id, String name, String title,String description, GroupType type, List<TeamMembershipGroup> teamMembershipGroups) {
        this.id = id;
        this.name = name;
        this.title = title;
        this.description = description;
        this.type = type;
        this.teamMembershipGroups = teamMembershipGroups;
    }

    public Group(String id, String name, String description, GroupType type) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.type = type;
    }

    // Getters and setters
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public GroupType getType() {
        return type;
    }

    public void setType(GroupType type) {
        this.type = type;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public List<TeamMembershipGroup> getTeamMembershipGroups() {
        return teamMembershipGroups;
    }

    public void setTeamMembershipGroups(List<TeamMembershipGroup> teamMembershipGroups) {
        this.teamMembershipGroups = teamMembershipGroups;
    }

    public List<GroupMembershipGroup> getGroupMembershipGroups() {
        return groupMembershipGroups;
    }

    public void setGroupMembershipGroups(List<GroupMembershipGroup> groupMembershipGroups) {
        this.groupMembershipGroups = groupMembershipGroups;
    }

    @Override
    public String toString() {
        return "Group{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", type='" + type + '\'' +
                '}';
    }
}