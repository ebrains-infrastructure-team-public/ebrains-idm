package eu.ebrains.idm.model.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.ArrayList;
import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Unit {
    private String id;

    private String title = "";

    private String name;

    private String description = "";

    // KC Attribute
    private boolean acceptMembershipRequest = false;

    private List<Unit> subUnits = new ArrayList<Unit>();

    public Unit(Unit unit) {
        this.id = unit.getId();
        this.title = unit.getTitle();
        this.name = unit.getName();
        this.description = unit.getDescription();
        this.acceptMembershipRequest = unit.isAcceptMembershipRequest();

        this.subUnits = unit.getSubUnits();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean isAcceptMembershipRequest() {
        return acceptMembershipRequest;
    }

    public void setAcceptMembershipRequest(boolean acceptMembershipRequest) {
        this.acceptMembershipRequest = acceptMembershipRequest;
    }

    public List<Unit> getSubUnits() {
        return subUnits;
    }

    public void setSubUnits(List<Unit> subUnits) {
        this.subUnits = subUnits;
    }
}
