package eu.ebrains.idm.model.normalized;

import jakarta.persistence.*;
import lombok.AccessLevel;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.FieldDefaults;

import java.io.Serializable;

@Data
@Entity
@Table(name = "GROUP_MEMBERSHIP_USER", uniqueConstraints = @UniqueConstraint(columnNames = {"GROUP_ID", "USER_ID", "GROUP_ROLE_ID" }))
@FieldDefaults(level = AccessLevel.PRIVATE)
@EqualsAndHashCode
public class GroupMembershipUserEntity extends AbstractEntity implements Serializable {

    @Id
    @Column(name="ID", length = 36)
    @Access(AccessType.PROPERTY)
    @GeneratedValue(strategy=GenerationType.UUID)
    String id;

    @ManyToOne(fetch= FetchType.LAZY)
    @JoinColumn(name = "GROUP_ID")
    GroupEntity group;

    @ManyToOne(fetch= FetchType.EAGER)
    @JoinColumn(name = "USER_ID")
    UserEntity user;

    @ManyToOne(fetch= FetchType.LAZY)
    @JoinColumn(name = "GROUP_ROLE_ID")
    GroupRoleEntity groupRole;

    public GroupMembershipUserEntity(GroupEntity group, UserEntity user, GroupRoleEntity groupRole) {
        this.group = group;
        this.user = user;
        this.groupRole = groupRole;
    }

    public GroupMembershipUserEntity() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public GroupEntity getGroup() {
        return group;
    }

    public void setGroup(GroupEntity groupEntity) {
        this.group = groupEntity;
    }

    public UserEntity getUser() {
        return user;
    }

    public void setUser(UserEntity user) {
        this.user = user;
    }

    public GroupRoleEntity getGroupRole() {
        return groupRole;
    }

    public void setGroupRole(GroupRoleEntity groupRole) {
        this.groupRole = groupRole;
    }
}
