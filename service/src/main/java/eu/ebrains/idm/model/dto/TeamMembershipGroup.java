package eu.ebrains.idm.model.dto;

public class TeamMembershipGroup {
    private String id;
    private String name;
    private Group group;
    private Team team;
    private TeamRole teamRole;

    public TeamMembershipGroup() {
    }

    public TeamMembershipGroup(String id, String name, Group group, Team team, TeamRole teamRole) {
        this.id = id;
        this.name = name;
        this.group = group;
        this.team = team;
        this.teamRole = teamRole;
    }

    public TeamMembershipGroup(String id, Group group, Team team, TeamRole teamRole) {
        this.id = id;
        this.group = group;
        this.team = team;
        this.teamRole = teamRole;
    }

    // Getters and setters
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Group getGroup() {
        return group;
    }

    public void setGroup(Group group) {
        this.group = group;
    }

    public Team getTeam() {
        return team;
    }

    public void setTeam(Team team) {
        this.team = team;
    }

    public TeamRole getTeamRole() {
        return teamRole;
    }

    public void setTeamRole(TeamRole teamRole) {
        this.teamRole = teamRole;
    }
}