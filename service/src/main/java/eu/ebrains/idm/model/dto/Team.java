package eu.ebrains.idm.model.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.util.List;

public class Team {
    private String id;
    private String name;
    private String description;
    @JsonIgnore
    private List<TeamMembershipGroup> teamMembershipGroups;
    @JsonIgnore
    private List<TeamMembershipUser> teamMembershipUsers;
    @JsonIgnore
    private String groupType;

    public Team() {
    }

    public Team(String id, String name, String description, List<TeamMembershipGroup> teamMembershipGroups, List<TeamMembershipUser> teamMembershipUsers) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.teamMembershipGroups = teamMembershipGroups;
        this.teamMembershipUsers = teamMembershipUsers;
    }

    public Team(String name, String description) {
        this.name = name;
        this.description = description;
    }

    public Team(String id, String name, String description, String groupType) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.groupType = groupType;
    }

    // Getters and setters
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<TeamMembershipGroup> getTeamMembershipGroups() {
        return teamMembershipGroups;
    }

    public void setTeamMembershipGroups(List<TeamMembershipGroup> teamMembershipGroups) {
        this.teamMembershipGroups = teamMembershipGroups;
    }

    public List<TeamMembershipUser> getTeamMembershipUsers() {
        return teamMembershipUsers;
    }

    public void setTeamMembershipUsers(List<TeamMembershipUser> teamMembershipUsers) {
        this.teamMembershipUsers = teamMembershipUsers;
    }

    public String getGroupType() {
        return groupType;
    }

    public void setGroupType(String groupType) {
        this.groupType = groupType;
    }
}