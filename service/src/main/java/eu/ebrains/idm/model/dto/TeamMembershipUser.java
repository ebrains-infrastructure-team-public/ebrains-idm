package eu.ebrains.idm.model.dto;

public class TeamMembershipUser {
    private String id;
    private User user;
    private Team team;
    private TeamRole teamRole;

    public TeamMembershipUser() {
    }

    public TeamMembershipUser(String id, User user, Team team, TeamRole teamRole) {
        this.id = id;
        this.user = user;
        this.team = team;
        this.teamRole = teamRole;
    }

    // Getters and setters
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Team getTeam() {
        return team;
    }

    public void setTeam(Team team) {
        this.team = team;
    }

    public TeamRole getTeamRole() {
        return teamRole;
    }

    public void setTeamRole(TeamRole teamRole) {
        this.teamRole = teamRole;
    }
}