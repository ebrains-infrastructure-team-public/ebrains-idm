package eu.ebrains.idm.model.normalized;

import jakarta.persistence.*;
import lombok.AccessLevel;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.FieldDefaults;

import java.io.Serializable;

@Data
@Entity
@Table(name = "TEAM_MEMBERSHIP_GROUP", uniqueConstraints = @UniqueConstraint(columnNames = {"GROUP_ID", "TEAM_ID", "TEAM_ROLE_ID"}))
@FieldDefaults(level = AccessLevel.PRIVATE)
@EqualsAndHashCode
public class TeamMembershipGroupEntity extends AbstractEntity implements Serializable {

    @Id
    @Column(name="ID", length = 36)
    @Access(AccessType.PROPERTY)
    @GeneratedValue(strategy=GenerationType.UUID)
    String id;

    @ManyToOne(fetch= FetchType.EAGER)
    @JoinColumn(name = "GROUP_ID")
    GroupEntity group;

    @ManyToOne(fetch= FetchType.LAZY)
    @JoinColumn(name = "TEAM_ID")
    TeamEntity team;

    @ManyToOne(fetch= FetchType.LAZY)
    @JoinColumn(name = "TEAM_ROLE_ID")
    TeamRoleEntity teamRole;

    public TeamMembershipGroupEntity(GroupEntity group, TeamEntity team, TeamRoleEntity teamRole) {
        this.group = group;
        this.team = team;
        this.teamRole = teamRole;
    }

    public TeamMembershipGroupEntity() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public GroupEntity getGroup() {
        return group;
    }

    public void setGroup(GroupEntity groupEntity) {
        this.group = groupEntity;
    }

    public TeamEntity getTeam() {
        return team;
    }

    public void setTeam(TeamEntity team) {
        this.team = team;
    }

    public TeamRoleEntity getTeamRole() {
        return teamRole;
    }

    public void setTeamRole(TeamRoleEntity teamRole) {
        this.teamRole = teamRole;
    }
}
