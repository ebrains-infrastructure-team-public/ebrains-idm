package eu.ebrains.idm.model.dto;

import eu.ebrains.idm.utils.enums.Role;

public class GroupRole {
    private Long id;
    private Role role;

    public GroupRole() {
    }

    public GroupRole(Long id, Role role) {
        this.id = id;
        this.role = role;
    }

    // Getters and setters
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }
}