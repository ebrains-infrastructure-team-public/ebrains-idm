package eu.ebrains.idm.model.dto;

public class GroupMembershipUser {
    private Group group;
    private User user;
    private GroupRole groupRole;

    public GroupMembershipUser() {
    }

    public GroupMembershipUser(Group group, User user, GroupRole groupRole) {
        this.group = group;
        this.user = user;
        this.groupRole = groupRole;
    }

    // Getters and setters
    public Group getGroup() {
        return group;
    }

    public void setGroup(Group group) {
        this.group = group;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public GroupRole getGroupRole() {
        return groupRole;
    }

    public void setGroupRole(GroupRole groupRole) {
        this.groupRole = groupRole;
    }
}