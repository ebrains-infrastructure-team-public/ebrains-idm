package eu.ebrains.idm.model.dto;

import java.util.ArrayList;
import java.util.List;

public class Membership
{
    private List<CollabUser> users = new ArrayList<CollabUser>();

    private List<Unit> units = new ArrayList<Unit>();

    private List<Group> groups = new ArrayList<Group>();

    public Membership()
    {
        super();
    }

    public List<CollabUser> getUsers()
    {
        return users;
    }

    public void setUsers(List<CollabUser> users)
    {
        this.users = users;
    }

    public List<Unit> getUnits()
    {
        return units;
    }

    public void setUnits(List<Unit> units)
    {
        this.units = units;
    }

    public List<Group> getGroups()
    {
        return groups;
    }

    public void setGroups(List<Group> groups)
    {
        this.groups = groups;
    }

    @Override
    public String toString() {
        return "Membership{" +
                "users=" + users +
                ", units=" + units +
                ", groups=" + groups +
                '}';
    }
}
