package eu.ebrains.idm.model.normalized;

import lombok.AccessLevel;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.FieldDefaults;

import jakarta.persistence.*;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Data
@Entity
@Table(name = "TEAM", uniqueConstraints = @UniqueConstraint(columnNames = {"NAME"}))
@FieldDefaults(level = AccessLevel.PRIVATE)
@EqualsAndHashCode
public class TeamEntity extends AbstractEntity implements Serializable {

    @Id
    @Column(name="ID", length = 36)
    @Access(AccessType.PROPERTY)
    @GeneratedValue(strategy = GenerationType.UUID)
    String id;

    @Column(name = "NAME",unique = true)
    String name;

    @Column(name = "DESCRIPTION")
    String description;

    // TODO: Add Title

    @OneToMany(mappedBy = "team", fetch = FetchType.LAZY, cascade = CascadeType.REMOVE)
    List<TeamMembershipGroupEntity> teamMembershipGroups = new ArrayList<TeamMembershipGroupEntity>();

    @OneToMany(mappedBy = "team", fetch = FetchType.LAZY, cascade = CascadeType.REMOVE)
    List<TeamMembershipUserEntity> teamMembershipUsers = new ArrayList<TeamMembershipUserEntity>();

    public TeamEntity(String id, String name, String description, List<TeamMembershipGroupEntity> teamMembershipGroups, List<TeamMembershipUserEntity> teamMembershipUsers) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.teamMembershipGroups = teamMembershipGroups;
        this.teamMembershipUsers = teamMembershipUsers;
    }

    public TeamEntity(String name, String description) {
        this.name = name;
        this.description = description;
    }

    public TeamEntity() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<TeamMembershipGroupEntity> getTeamMembershipGroups() {
        return teamMembershipGroups;
    }

    public void setTeamMembershipGroups(List<TeamMembershipGroupEntity> teamMembershipGroups) {
        if(teamMembershipGroups!=null) {
            this.teamMembershipGroups.clear();
            this.teamMembershipGroups.addAll(teamMembershipGroups);
        }
    }

    public List<TeamMembershipUserEntity> getTeamMembershipUsers() {
        return teamMembershipUsers;
    }

    public void setTeamMembershipUsers(List<TeamMembershipUserEntity> teamMembershipUsers) {
        if(teamMembershipUsers!=null) {
            this.teamMembershipUsers.clear();
            this.teamMembershipUsers.addAll(teamMembershipUsers);
        }
    }
}
