package eu.ebrains.idm.model.normalized;

import eu.ebrains.idm.utils.converter.GroupTypeConverter;
import eu.ebrains.idm.utils.converter.RoleConverter;
import eu.ebrains.idm.utils.enums.GroupType;
import eu.ebrains.idm.utils.enums.Role;
import lombok.AccessLevel;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.FieldDefaults;

import jakarta.persistence.*;

import java.io.Serializable;
import java.util.List;

@Data
@Entity
@Table(name = "GROUP_TABLE", uniqueConstraints = @UniqueConstraint(columnNames = {"ID", "NAME", "TITLE"}))
@FieldDefaults(level = AccessLevel.PRIVATE)
@EqualsAndHashCode
public class GroupEntity extends AbstractEntity implements Serializable {

    @Id
    @Column(name="ID", length = 36)
    @Access(AccessType.PROPERTY)
    @GeneratedValue(strategy=GenerationType.UUID)
    String id;

    @Column(name = "NAME")
    String name;

    @Column(name = "TITLE")
    String title;

    @Column(name = "DESCRIPTION")
    String description;

    @Column(name = "TYPE")
    @Convert(converter = GroupTypeConverter.class)
    GroupType type;

    @OneToMany(mappedBy = "group", fetch = FetchType.LAZY)
    List<TeamMembershipGroupEntity> teamMembershipGroups;

    @OneToMany(mappedBy = "childGroup", fetch = FetchType.LAZY)
    List<GroupMembershipGroupEntity> groupMembershipGroups;

    public GroupEntity(String name, String title, String description, GroupType type, List<TeamMembershipGroupEntity> teamMembershipGroups) {
        this.name = name;
        this.title= title;
        this.description = description;
        this.type = type;
        this.teamMembershipGroups = teamMembershipGroups;
    }

    public GroupEntity(String name, String title, String description, GroupType type) {
        this.name = name;
        this.title= title;
        this.description = description;
        this.type = type;
    }

    public GroupEntity() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public GroupType getType() {
        return type;
    }

    public void setType(GroupType type) {
        this.type = type;
    }

    public List<TeamMembershipGroupEntity> getTeamMembershipGroups() {
        return teamMembershipGroups;
    }

    public void setTeamMembershipGroups(List<TeamMembershipGroupEntity> teamMembershipGroups) {
        this.teamMembershipGroups = teamMembershipGroups;
    }


    public List<GroupMembershipGroupEntity> getGroupMembershipGroups() {
        return groupMembershipGroups;
    }

    public void setGroupMembershipGroups(List<GroupMembershipGroupEntity> groupMembershipGroups) {
        this.groupMembershipGroups = groupMembershipGroups;
    }
}
