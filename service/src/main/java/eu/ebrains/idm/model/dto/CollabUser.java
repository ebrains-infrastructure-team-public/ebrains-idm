package eu.ebrains.idm.model.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonIgnore;

@JsonIgnoreProperties(ignoreUnknown = true)
public class CollabUser {
    public static final String GUEST_PREFIX = "guest";

    private String id;

    private String mitreId;

    private String username;

    private String firstName;

    private String lastName;

    private String email;

    private String biography = "";

    private String avatar = "";

    @JsonIgnore
    private String lastVerification;

    private boolean active = false;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getMitreId() {
        return mitreId;
    }

    public void setMitreId(String mitreId) {
        this.mitreId = mitreId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getBiography() {
        return biography;
    }

    public void setBiography(String biography) {
        this.biography = biography;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getLastVerification() {
        return lastVerification;
    }

    public void setLastVerification(String lastVerification) {
        this.lastVerification = lastVerification;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    @Override
    public String toString() {
        return "CollabUser{" +
                "id='" + id + '\'' +
                ", mitreId='" + mitreId + '\'' +
                ", username='" + username + '\'' +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", email='" + email + '\'' +
                ", biography='" + biography + '\'' +
                ", avatar='" + avatar + '\'' +
                ", lastVerification='" + lastVerification + '\'' +
                ", active=" + active +
                '}';
    }
}