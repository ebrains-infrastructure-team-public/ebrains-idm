package eu.ebrains.idm.model.normalized;

import jakarta.persistence.*;
import lombok.AccessLevel;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.FieldDefaults;

import java.io.Serializable;

@Data
@Entity
@Table(name = "TEAM_MEMBERSHIP_USER",uniqueConstraints = @UniqueConstraint(columnNames = {"TEAM_ID", "USER_ID", "TEAM_ROLE_ID"}))
@FieldDefaults(level = AccessLevel.PRIVATE)
@EqualsAndHashCode
public class TeamMembershipUserEntity extends AbstractEntity implements Serializable {

    @Id
    @Column(name="ID", length = 36)
    @Access(AccessType.PROPERTY)
    @GeneratedValue(strategy=GenerationType.UUID)
    String id;

    @ManyToOne(fetch= FetchType.EAGER)
    @JoinColumn(name = "USER_ID")
    UserEntity user;

    @ManyToOne(fetch= FetchType.LAZY)
    @JoinColumn(name = "TEAM_ID")
    TeamEntity team;

    @ManyToOne(fetch= FetchType.LAZY)
    @JoinColumn(name = "TEAM_ROLE_ID")
    TeamRoleEntity teamRole;

    public TeamMembershipUserEntity(UserEntity user, TeamEntity team, TeamRoleEntity teamRole) {
        this.user = user;
        this.team = team;
        this.teamRole = teamRole;
    }

    public TeamMembershipUserEntity() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public UserEntity getUser() {
        return user;
    }

    public void setUser(UserEntity user) {
        this.user = user;
    }

    public TeamEntity getTeam() {
        return team;
    }

    public void setTeam(TeamEntity team) {
        this.team = team;
    }

    public TeamRoleEntity getTeamRole() {
        return teamRole;
    }

    public void setTeamRole(TeamRoleEntity teamRole) {
        this.teamRole = teamRole;
    }
}
