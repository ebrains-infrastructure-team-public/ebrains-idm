package eu.ebrains.idm.model.normalized;

import eu.ebrains.idm.utils.converter.RoleConverter;
import eu.ebrains.idm.utils.enums.Role;
import lombok.AccessLevel;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.FieldDefaults;

import jakarta.persistence.*;

import java.io.Serializable;

@Data
@Entity
@Table(name = "TEAM_ROLE")
@FieldDefaults(level = AccessLevel.PRIVATE)
@EqualsAndHashCode
public class TeamRoleEntity implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    Long id;

    @Enumerated(EnumType.STRING)
    @Column(name = "ROLE")
    @Convert(converter = RoleConverter.class)
    Role role;

    public TeamRoleEntity(Role role) {
        this.role = role;
    }

    public TeamRoleEntity() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }
}
