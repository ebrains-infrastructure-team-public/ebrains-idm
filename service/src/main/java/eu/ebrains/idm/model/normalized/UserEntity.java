package eu.ebrains.idm.model.normalized;

import jakarta.persistence.*;
import lombok.AccessLevel;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.FieldDefaults;

import java.io.Serializable;

@Data
@Entity
@Table(name = "USER", uniqueConstraints = @UniqueConstraint(columnNames = {"KEYCLOAK_SUB", "USERNAME", "EMAIL_ADDRESS"}))
@FieldDefaults(level = AccessLevel.PRIVATE)
@EqualsAndHashCode
public class UserEntity extends AbstractEntity implements Serializable {

    @Id
    @Column(name="ID", length = 36)
    @Access(AccessType.PROPERTY)
    @GeneratedValue(strategy = GenerationType.UUID)
    String id;

    @Column(name = "KEYCLOAK_SUB")
    String sub;

    @Column(name = "USERNAME")
    String username;

    @Column(name = "FIRSTNAME")
    String firstname;

    @Column(name = "LASTNAME")
    String lastname;

    @Column(name = "EMAIL_ADDRESS")
    String emailAddress;

    @Column(name = "IS_GUEST")
    Boolean isGuest;

    public UserEntity(String username, String emailAddress, Boolean isGuest) {
        this.username = username;
        this.emailAddress = emailAddress;
        this.isGuest = isGuest;
    }

    public UserEntity(String username, String sub) {
        this.username = username;
        this.sub = sub;
    }

    public UserEntity(String id, String sub, String username, String firstname, String lastname, String emailAddress, Boolean isGuest) {
        this.id = id;
        this.sub = sub;
        this.username = username;
        this.firstname = firstname;
        this.lastname = lastname;
        this.emailAddress = emailAddress;
        this.isGuest = isGuest;
    }

    public UserEntity() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public Boolean getGuest() {
        return isGuest;
    }

    public void setGuest(Boolean guest) {
        isGuest = guest;
    }

    public String getSub() {
        return sub;
    }

    public void setSub(String sub) {
        this.sub = sub;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }
}
