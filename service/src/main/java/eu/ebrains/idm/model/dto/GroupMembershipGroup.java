package eu.ebrains.idm.model.dto;

public class GroupMembershipGroup {
    private Group parentGroup;
    private Group childGroup;
    private GroupRole groupRole;

    public GroupMembershipGroup() {
    }

    public GroupMembershipGroup(Group parentGroup, Group childGroup, GroupRole groupRole) {
        this.parentGroup = parentGroup;
        this.childGroup = childGroup;
        this.groupRole = groupRole;
    }

    // Getters and setters
    public Group getParentGroup() {
        return parentGroup;
    }

    public void setParentGroup(Group parentGroup) {
        this.parentGroup = parentGroup;
    }

    public Group getChildGroup() {
        return childGroup;
    }

    public void setChildGroup(Group childGroup) {
        this.childGroup = childGroup;
    }

    public GroupRole getGroupRole() {
        return groupRole;
    }

    public void setGroupRole(GroupRole groupRole) {
        this.groupRole = groupRole;
    }
}