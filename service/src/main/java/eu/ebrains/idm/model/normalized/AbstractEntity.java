package eu.ebrains.idm.model.normalized;

import eu.ebrains.idm.configuration.CustomEntityListener;
import jakarta.persistence.*;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

import java.util.Date;

@MappedSuperclass
@EntityListeners(CustomEntityListener.class)
public abstract class AbstractEntity {

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "DATE_CREATION", updatable = false)
    private Date dateCreation;

    @Column(name = "USER_CREATION", updatable = false)
    private String userCreation;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "DATE_UPDATE", nullable = true, insertable = false)
    private Date dateUpdate;

    @Column(name = "USER_UPDATE", nullable = true, insertable = false)
    private String userUpdate;

    public Date getDateCreation() {
        return dateCreation;
    }

    public void setDateCreation(Date dateCreation) {
        this.dateCreation = dateCreation;
    }

    public String getUserCreation() {
        return userCreation;
    }

    public void setUserCreation(String userCreation) {
        this.userCreation = userCreation;
    }

    public Date getDateUpdate() {
        return dateUpdate;
    }

    public void setDateUpdate(Date dateUpdate) {
        this.dateUpdate = dateUpdate;
    }

    public String getUserUpdate() {
        return userUpdate;
    }

    public void setUserUpdate(String userUpdate) {
        this.userUpdate = userUpdate;
    }
}