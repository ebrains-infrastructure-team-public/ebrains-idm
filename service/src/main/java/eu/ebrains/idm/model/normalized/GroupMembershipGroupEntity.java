package eu.ebrains.idm.model.normalized;

import lombok.AccessLevel;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.FieldDefaults;

import jakarta.persistence.*;
import java.io.Serializable;

@Data
@Entity
@Table(name = "GROUP_MEMBERSHIP_GROUP", uniqueConstraints = @UniqueConstraint(columnNames = {"PARENT_GROUP_ID", "CHILD_GROUP_ID", "GROUP_ROLE_ID" }))
@FieldDefaults(level = AccessLevel.PRIVATE)
@EqualsAndHashCode
public class GroupMembershipGroupEntity extends AbstractEntity implements Serializable {

    @Id
    @Column(name="ID", length = 36)
    @Access(AccessType.PROPERTY)
    @GeneratedValue(strategy=GenerationType.UUID)
    String id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "PARENT_GROUP_ID")
    GroupEntity parentGroup;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "CHILD_GROUP_ID")
    GroupEntity childGroup;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "GROUP_ROLE_ID")
    GroupRoleEntity groupRole;

    public GroupMembershipGroupEntity(GroupEntity parentGroup, GroupEntity childGroup, GroupRoleEntity groupRole) {
        this.parentGroup = parentGroup;
        this.childGroup = childGroup;
        this.groupRole = groupRole;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public GroupMembershipGroupEntity() {
    }

    public GroupEntity getParentGroup() {
        return parentGroup;
    }

    public void setParentGroup(GroupEntity parentGroup) {
        this.parentGroup = parentGroup;
    }

    public GroupEntity getChildGroup() {
        return childGroup;
    }

    public void setChildGroup(GroupEntity childGroup) {
        this.childGroup = childGroup;
    }

    public GroupRoleEntity getGroupRole() {
        return groupRole;
    }

    public void setGroupRole(GroupRoleEntity groupRole) {
        this.groupRole = groupRole;
    }
}