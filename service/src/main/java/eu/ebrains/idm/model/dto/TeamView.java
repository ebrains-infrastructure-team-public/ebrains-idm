package eu.ebrains.idm.model.dto;

public class TeamView {
    private String id;
    private String name;
    private String description;
    private String groupType;

    public TeamView() {
    }

    public TeamView(String name, String description) {
        this.name = name;
        this.description = description;
    }

    public TeamView(String id, String name, String description, String groupType) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.groupType = groupType;
    }

    // Getters and setters
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getGroupType() {
        return groupType;
    }

    public void setGroupType(String groupType) {
        this.groupType = groupType;
    }
}