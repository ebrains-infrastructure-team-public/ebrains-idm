package eu.ebrains.idm.exception;

public class EnumNotFoundException extends RuntimeException{

    private static final long serialVersionUID = 1L;

    public EnumNotFoundException(String msg) {
        super(msg);
    }
}
