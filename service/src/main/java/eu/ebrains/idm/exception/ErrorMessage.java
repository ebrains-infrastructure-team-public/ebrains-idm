package eu.ebrains.idm.exception;

import java.util.Date;

public class ErrorMessage {
    private int statusCode;
    private Date timestamp;
    private String message;

    public static final String WRONG_ROLE_SPECIFIED_ERROR_MSG = "Role should be viewer, editor or administrator.";
    public static final String TEAM_NAME_EMPTY_ERROR_MSG = "Team name cannot be empty.";
    public static final String TEAM_DESCRIPTION_EMPTY_ERROR_MSG = "Team description cannot be empty.";
    public static final String TEAM_NAME_SHORT_OR_LONG_ERROR_MSG = "Team name must be 5 to 40 characters long.";
    public static final String TEAM_NAME_WITH_SPECIAL_CHARS_ERROR_MSG = "Team name cannot contain theses characters + # / \\ ; @ < > \"";
    public static final String USER_NOT_FOUND_ERROR_MSG = "No user found with the following username : %s";
    public static final String TEAM_NOT_FOUND_ERROR_MSG = "No team found with the following name : %s";
    public static final String GROUP_NOT_FOUND_ERROR_MSG = "No group found with the following name : %s";
    public static final String TEAM_ALREADY_EXIST_ERROR_MSG = "Another team exists with the same name.";
    public static final String GROUP_ALREADY_EXIST_ERROR_MSG = "Another group exists with the same name.";
    public static final String USER_LOWER_ROLE_CHANGE_ERROR_MSG = "This user already has a higher level role, please remove them from this role before giving them the new role.";
    public static final String USER_SAME_ROLE_CHANGE_ERROR_MSG = "This user already has this role level.";
    public static final String GROUP_LOWER_ROLE_CHANGE_ERROR_MSG = "This group already has a higher level role, please remove them from this role before giving them the new role.";
    public static final String GROUP_SAME_ROLE_CHANGE_ERROR_MSG = "This group already has this role level.";
    public static final String UNKNOW_DATABASE_VALUE_ERROR_MSG = "Unknown database value: %s";
    public static final String GROUP_TYPE_NOT_FOUND_ERROR_MSG = "No GroupType found with the following key : %s";
    public static final String ROLE_NOT_FOUND_ERROR_MSG = "No Role found with the following key : %s";
    public static final String USER_NOT_MEMBER_OF_TEAM_ERROR_MSG = "The user %s is not a member of the team %s";
    public static final String USER_NOT_MEMBER_OF_GROUP_ERROR_MSG = "The user %s is not a member of the group %s";
    public static final String USER_HAS_DIFFERENT_ROLE_OF_TEAM_ERROR_MSG = "The user %s is member of the team %s but has another role assigned.";
    public static final String USER_HAS_DIFFERENT_ROLE_OF_GROUP_ERROR_MSG = "The user %s is member of the group %s but has another role assigned.";
    public static final String GROUP_NOT_MEMBER_OF_TEAM_ERROR_MSG = "The group %s is not a member of the team %s";
    public static final String GROUP_NOT_MEMBER_OF_GROUP_ERROR_MSG = "The group %s is not a member of the group %s";
    public static final String GROUP_HAS_DIFFERENT_ROLE_OF_TEAM_ERROR_MSG = "The group %s is member of the team %s but has another role assigned.";
    public static final String GROUP_HAS_DIFFERENT_ROLE_OF_GROUP_ERROR_MSG = "The group %s is member of the group %s but has another role assigned.";
    public static final String MUST_BE_A_GROUP_ADMIN_MSG = "You must be an administrator of this group.";
    public static final String MUST_BE_A_GROUP_MEMBER_OR_ADMIN_MSG = "You must be an administrator or member of this group.";
    public static final String LOGIN_REQUIRED_MSG = "Login required.";

    // API Error messages
    public static final String API_SUCCESS_MSG = "Success";
    public static final String API_CREATED_MSG = "Created";
    public static final String API_NO_CONTENT_MSG = "No content";
    public static final String API_LOGIN_REQUIRED_ERROR_MSG = "Login required";
    public static final String API_TEAM_NOT_FOUND_ERROR_MSG = "Team not found";
    public static final String API_TEAM_ALREADY_EXISTS = "Team already exists";
    public static final String API_GROUP_ALREADY_EXISTS = "Group already exists";
    public static final String API_USER_NOT_FOUND_ERROR_MSG = "User not found";
    public static final String API_GROUP_NOT_FOUND_ERROR_MSG = "Group not found";
    public static final String API_UNIT_NOT_FOUND_ERROR_MSG = "Unit not found";
    public static final String API_USER_NOT_MEMBER_OF_TEAM_ERROR_MSG = "User not member of team";
    public static final String API_GROUP_NOT_MEMBER_OF_TEAM_ERROR_MSG = "Group not member of team";
    public static final String API_UNIT_NOT_MEMBER_OF_TEAM_ERROR_MSG = "Unit not member of team";
    public static final String API_USER_NOT_MEMBER_OF_GROUP_ERROR_MSG = "User not member of group";
    public static final String API_USER_NOT_MEMBER_OF_UNIT_ERROR_MSG = "User not member of unit";
    public static final String API_GROUP_NOT_MEMBER_OF_GROUP_ERROR_MSG = "Group not member of group";
    public static final String API_UNIT_NOT_MEMBER_OF_GROUP_ERROR_MSG = "Unit not member of group";

    public ErrorMessage(int statusCode, Date timestamp, String message) {
        this.statusCode = statusCode;
        this.timestamp = timestamp;
        this.message = message;
    }

    public ErrorMessage() {
        super();
    }

    public int getStatusCode() {
        return statusCode;
    }

    public Date getTimestamp() {
        return timestamp;
    }

    public String getMessage() {
        return message;
    }
}
