package eu.ebrains.idm.exception;

public class RoleChangeException extends RuntimeException{

    private static final long serialVersionUID = 1L;

    public RoleChangeException(String msg) {
        super(msg);
    }
}
