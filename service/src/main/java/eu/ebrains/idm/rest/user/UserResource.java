package eu.ebrains.idm.rest.user;

import eu.ebrains.idm.mapper.CollabUserMapper;
import eu.ebrains.idm.model.dto.CollabUser;
import eu.ebrains.idm.model.dto.User;
import eu.ebrains.idm.service.UserService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import javax.ws.rs.DefaultValue;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import java.util.List;

import static eu.ebrains.idm.exception.ErrorMessage.*;

@Tag(name = "User")
@RestController
@RequestMapping("/users")
public class UserResource {

    private static final Logger logger = LoggerFactory.getLogger(UserResource.class);

    @Autowired
    private UserService userService;

    @Autowired
    private CollabUserMapper collabUserMapper;

    public static final int SEARCH_USER_LIMIT_RESULT = 30;

    @GetMapping("/{userId}")
    public User getUserById(@PathVariable String userId) {
        return userService.getUserById(userId);
    }


    /**
     * Futur userinfo endpoint that will return equivalent of OIDC /userinfo adding teams/groups info from the IDM
     * It's just a POC right now to test authorization
     * Instead of hasAuthority that check specific scope you can just put "isAuthenticated" instead if a valid token is the only thing we need to check
     * @return
     */
    @GetMapping("/userinfo")
    @PreAuthorize("hasAuthority('SCOPE_openid')")
    public Authentication getUserById() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        logger.info("test");
        return authentication;
    }
    @PostMapping
    public User createUser(@RequestBody User user) {
        return userService.createUser(user);
    }

    @PutMapping("/{userId}")
    public User updateUser(@PathVariable String userId, @RequestBody User user) {
        return userService.updateUser(userId, user);
    }

    @DeleteMapping("/{userId}")
    public void deleteUser(@PathVariable String userId) {
        userService.deleteUser(userId);
    }

    @Operation(description = "Get a user by username or iam sub, units and groups of the user are not returned in this API.", summary = "Get a user by username or sub", responses = {
            @ApiResponse(responseCode = "200", description = API_SUCCESS_MSG),
            @ApiResponse(responseCode = "401", description = API_LOGIN_REQUIRED_ERROR_MSG),
            @ApiResponse(responseCode = "404", description = API_USER_NOT_FOUND_ERROR_MSG)
    })
    @GetMapping("/{username}")
    public ResponseEntity<CollabUser> getUser(@PathParam("username") String username) {
        // TODO: 31/12/2023 Don't forget to implement get user by sub logic 
        CollabUser user = collabUserMapper.userToCollabUser(userService.getUserByUsername(username));
        return ResponseEntity.ok(user);
    }

    @Operation(description = "Get the email of a user, this API is accessible only through for authorized users.", summary = "Get the email of a user", responses = {
            @ApiResponse(responseCode = "200", description = API_SUCCESS_MSG, content = @Content(mediaType = MediaType.TEXT_PLAIN)),
            @ApiResponse(responseCode = "401", description = API_LOGIN_REQUIRED_ERROR_MSG)})
    @GetMapping("/{username}/email")
    @Produces(MediaType.TEXT_PLAIN)
    public ResponseEntity<String> getUserEmail(@PathParam("username") String username) {

//        if(!userService.isUserAuthorizedToReadUserEmail(userReference.getName())) {
//            throw new AuthorizationException("This API is only usable for authorized users, if you think your should be authorized, please contact our support.");
//        }

        return ResponseEntity.ok(userService.getUserByUsername(username).getEmailAddress());
    }

    @Operation(description = "Search a user by username, first name, last name or email", summary = "Search a user by username, first name, last name or email", responses = {
            @ApiResponse(responseCode = "200", description = API_SUCCESS_MSG, content = @Content(mediaType = "application/json", array = @ArraySchema(schema = @Schema(implementation = CollabUser.class)))),
            @ApiResponse(responseCode = "401", description = API_LOGIN_REQUIRED_ERROR_MSG)})
    @GetMapping
    @Produces(MediaType.APPLICATION_JSON)
    public ResponseEntity<List<CollabUser>> findUsers(@QueryParam("search") @DefaultValue("") String search, @QueryParam("sub") @DefaultValue("") String sub) {

        return ResponseEntity.ok(userService.searchUser(search, SEARCH_USER_LIMIT_RESULT));
    }
}
