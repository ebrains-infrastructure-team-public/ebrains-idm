package eu.ebrains.idm.rest.team;

import eu.ebrains.idm.model.dto.Group;

import java.util.List;

public class CreateTeamResponse {

    private String id;
    private String name;
    private String description;

    public CreateTeamResponse(String id, String name, String description) {
        this.id = id;
        this.name = name;
        this.description = description;
    }

    public CreateTeamResponse() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
