package eu.ebrains.idm.rest.team;

import com.google.common.base.Strings;
import eu.ebrains.idm.exception.EnumNotFoundException;
import eu.ebrains.idm.exception.ErrorMessage;
import eu.ebrains.idm.exception.InvalidParameterException;
import eu.ebrains.idm.model.dto.*;
import eu.ebrains.idm.service.TeamService;
import eu.ebrains.idm.utils.converter.RoleConverter;
import eu.ebrains.idm.utils.enums.Role;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static eu.ebrains.idm.exception.ErrorMessage.*;
import static eu.ebrains.idm.utils.IDMUtils.checkTeamRole;

@Tag(name = "Team")
@RestController
@RequestMapping("/teams")
public class TeamResource {

    @Autowired
    private TeamService teamService;

    @Operation(description = "Get a list of teams", summary = "Get a list of teams", responses = {
            @ApiResponse(responseCode = "200", description = API_SUCCESS_MSG, content = @Content(mediaType = "application/json", array = @ArraySchema(schema = @Schema(implementation = Team.class)))),
            @ApiResponse(responseCode = "401", description = API_LOGIN_REQUIRED_ERROR_MSG)
    })
    @GetMapping
    public ResponseEntity<List<Team>> getAllTeams() {
        return ResponseEntity.ok(teamService.getAllTeams());
    }

    @Operation(description = "Get a team by Id", summary = "Get a team by Id", responses = {
            @ApiResponse(responseCode = "200", description = API_SUCCESS_MSG, content = @Content(mediaType = "application/json", schema = @Schema(implementation = Team.class))),
            @ApiResponse(responseCode = "401", description = API_LOGIN_REQUIRED_ERROR_MSG),
            @ApiResponse(responseCode = "404", description = API_TEAM_NOT_FOUND_ERROR_MSG)
    })
    @GetMapping("/{teamId}")
    public ResponseEntity<Team> getTeamById(@PathVariable String teamId) {
        return ResponseEntity.ok(teamService.getTeamById(teamId));
    }

    @Operation(description = "Get a list of users, units and groups for a team, only returns users/units/groups with the given role", summary = "Get a list of users, units and groups on the team with the given role", responses = {
            @ApiResponse(responseCode = "200", description = API_SUCCESS_MSG, content = @Content(mediaType = "application/json", schema = @Schema(implementation = Membership.class))),
            @ApiResponse(responseCode = "401", description = API_LOGIN_REQUIRED_ERROR_MSG),
            @ApiResponse(responseCode = "404", description = API_TEAM_NOT_FOUND_ERROR_MSG)
    })
    @GetMapping("/{teamId}/{role}")
    public ResponseEntity<Membership> getTeamMembershipByRole(@PathVariable String teamId, @PathVariable String role) {
        return ResponseEntity.ok(teamService.getTeamMembershipByRole(teamId, Role.fromKey(role)));
    }

    @Operation(description = "Create a new team", summary = "Create a new team", responses = {
            @ApiResponse(responseCode = "200", description = API_SUCCESS_MSG, content = @Content(mediaType = "application/json", schema = @Schema(implementation = CreateTeamResponse.class))),
            @ApiResponse(responseCode = "401", description = API_LOGIN_REQUIRED_ERROR_MSG),
            @ApiResponse(responseCode = "409", description = API_TEAM_ALREADY_EXISTS)
    })
    @PostMapping
    @PreAuthorize("hasAuthority('SCOPE_openid')")
    public ResponseEntity<CreateTeamResponse> createTeam(@RequestBody CreateTeamForm form) {
        checkCreateTeamForm(form);
        return ResponseEntity.ok(teamService.createTeam(form));
    }

    @Operation(description = "Update an existing team", summary = "Update an existing team", responses = {
            @ApiResponse(responseCode = "200", description = API_SUCCESS_MSG, content = @Content(mediaType = "application/json", schema = @Schema(implementation = Team.class))),
            @ApiResponse(responseCode = "401", description = API_LOGIN_REQUIRED_ERROR_MSG),
            @ApiResponse(responseCode = "404", description = API_TEAM_NOT_FOUND_ERROR_MSG)
    })
    @PutMapping("/{teamId}")
    public ResponseEntity<Team> updateTeam(@PathVariable String teamId, @RequestBody Team team) {
        return ResponseEntity.ok(teamService.updateTeam(teamId, team));
    }

    @Operation(description = "Delete an existing team by name", summary = "Delete an existing team by name", responses = {
            @ApiResponse(responseCode = "200", description = API_SUCCESS_MSG),
            @ApiResponse(responseCode = "401", description = API_LOGIN_REQUIRED_ERROR_MSG),
            @ApiResponse(responseCode = "404", description = API_TEAM_NOT_FOUND_ERROR_MSG)
    })
    @DeleteMapping("/{teamName}")
    public ResponseEntity deleteTeam(@PathVariable String teamName) {
        teamService.deleteTeam(teamName);
        return ResponseEntity.ok().build();
    }

    @Operation(description = "Delete a user from team with the role specified", summary = "Delete a user from team with the role specified", responses = {
            @ApiResponse(responseCode = "200", description = API_SUCCESS_MSG),
            @ApiResponse(responseCode = "401", description = API_LOGIN_REQUIRED_ERROR_MSG),
            @ApiResponse(responseCode = "404", description = API_TEAM_NOT_FOUND_ERROR_MSG + "<br/>"
                    + API_USER_NOT_FOUND_ERROR_MSG + "<br/>"
                    + API_USER_NOT_MEMBER_OF_TEAM_ERROR_MSG),
            @ApiResponse(responseCode = "406", description = WRONG_ROLE_SPECIFIED_ERROR_MSG)
    })
    @DeleteMapping("/{teamName}/{role}/users/{username}")
    public ResponseEntity deleteUserFromTeam(@PathVariable String teamName, @PathVariable String role, @PathVariable String username) {
        if (!checkTeamRole(role)) {
            throw new EnumNotFoundException(ErrorMessage.WRONG_ROLE_SPECIFIED_ERROR_MSG);
        }
        teamService.deleteUserFromTeam(teamName, username, role);
        return ResponseEntity.ok().build();
    }

    @Operation(description = "Delete a group from team with the role specified", summary = "Delete a group from team with the role specified", responses = {
            @ApiResponse(responseCode = "200", description = API_SUCCESS_MSG),
            @ApiResponse(responseCode = "401", description = API_LOGIN_REQUIRED_ERROR_MSG),
            @ApiResponse(responseCode = "404", description = API_TEAM_NOT_FOUND_ERROR_MSG + "<br/>"
                    + API_GROUP_NOT_FOUND_ERROR_MSG + "<br/>"
                    + API_GROUP_NOT_MEMBER_OF_TEAM_ERROR_MSG),
            @ApiResponse(responseCode = "406", description = WRONG_ROLE_SPECIFIED_ERROR_MSG)
    })
    @DeleteMapping("/{teamName}/{role}/groups/{groupName}")
    public ResponseEntity deleteGroupFromTeam(@PathVariable String teamName, @PathVariable String role, @PathVariable String groupName) {
        if (!checkTeamRole(role)) {
            throw new EnumNotFoundException(ErrorMessage.WRONG_ROLE_SPECIFIED_ERROR_MSG);
        }
        teamService.deleteGroupFromTeam(teamName, groupName, role);
        return ResponseEntity.ok().build();
    }

    @Operation(description = "Delete a unit from team with the role specified", summary = "Delete a unit from team with the role specified", responses = {
            @ApiResponse(responseCode = "200", description = API_SUCCESS_MSG),
            @ApiResponse(responseCode = "401", description = API_LOGIN_REQUIRED_ERROR_MSG),
            @ApiResponse(responseCode = "404", description = API_TEAM_NOT_FOUND_ERROR_MSG + "<br/>"
                    + API_UNIT_NOT_FOUND_ERROR_MSG + "<br/>"
                    + API_UNIT_NOT_MEMBER_OF_TEAM_ERROR_MSG),
            @ApiResponse(responseCode = "406", description = WRONG_ROLE_SPECIFIED_ERROR_MSG)
    })
    @DeleteMapping("/{teamName}/{role}/units/{unitName}")
    public ResponseEntity deleteUnitFromTeam(@PathVariable String teamName, @PathVariable String role, @PathVariable String unitName) {
        if (!checkTeamRole(role)) {
            throw new EnumNotFoundException(ErrorMessage.WRONG_ROLE_SPECIFIED_ERROR_MSG);
        }
        teamService.deleteGroupFromTeam(teamName, unitName, role);
        return ResponseEntity.ok().build();
    }

    @Operation(description = "Add a user to a team with the specified role", summary = "Add a user to a team with the specified role", responses = {
            @ApiResponse(responseCode = "204", description = API_NO_CONTENT_MSG),
            @ApiResponse(responseCode = "401", description = API_LOGIN_REQUIRED_ERROR_MSG),
            @ApiResponse(responseCode = "404", description = API_TEAM_NOT_FOUND_ERROR_MSG + "<br/>"
                    + API_USER_NOT_FOUND_ERROR_MSG + "<br/>"
                    + API_USER_NOT_MEMBER_OF_TEAM_ERROR_MSG),
            @ApiResponse(responseCode = "406", description = WRONG_ROLE_SPECIFIED_ERROR_MSG)
    })
    @PutMapping("/{teamName}/{role}/users/{username}")
    public ResponseEntity addUserToTeam(@PathVariable String teamName, @PathVariable String role, @PathVariable String username) {
        if (!checkTeamRole(role)) {
            throw new EnumNotFoundException(ErrorMessage.WRONG_ROLE_SPECIFIED_ERROR_MSG);
        }
        teamService.addUserToTeam(teamName, role, username);
        return ResponseEntity.noContent().build();
    }

    @Operation(description = "Add a group to a team with the specified role", summary = "Add a group to a team with the specified role", responses = {
            @ApiResponse(responseCode = "204", description = API_NO_CONTENT_MSG),
            @ApiResponse(responseCode = "401", description = API_LOGIN_REQUIRED_ERROR_MSG),
            @ApiResponse(responseCode = "404", description = API_TEAM_NOT_FOUND_ERROR_MSG + "<br/>"
                    + API_GROUP_NOT_FOUND_ERROR_MSG + "<br/>"
                    + API_GROUP_NOT_MEMBER_OF_TEAM_ERROR_MSG),
            @ApiResponse(responseCode = "406", description = WRONG_ROLE_SPECIFIED_ERROR_MSG)
    })
    @PutMapping("/{teamName}/{role}/groups/{groupName}")
    public ResponseEntity addGroupToTeam(@PathVariable String teamName, @PathVariable String role, @PathVariable String groupName) {
        if (!checkTeamRole(role)) {
            throw new EnumNotFoundException(ErrorMessage.WRONG_ROLE_SPECIFIED_ERROR_MSG);
        }
        teamService.addGroupToTeam(teamName, role, groupName);
        return ResponseEntity.noContent().build();
    }

    @Operation(description = "Add a unit to a team with the specified role", summary = "Add a unit to a team with the specified role", responses = {
            @ApiResponse(responseCode = "204", description = API_NO_CONTENT_MSG),
            @ApiResponse(responseCode = "401", description = API_LOGIN_REQUIRED_ERROR_MSG),
            @ApiResponse(responseCode = "404", description = API_TEAM_NOT_FOUND_ERROR_MSG + "<br/>"
                    + API_UNIT_NOT_FOUND_ERROR_MSG + "<br/>"
                    + API_UNIT_NOT_MEMBER_OF_TEAM_ERROR_MSG),
            @ApiResponse(responseCode = "406", description = WRONG_ROLE_SPECIFIED_ERROR_MSG)
    })
    @PutMapping("/{teamName}/{role}/units/{unitName}")
    public ResponseEntity addUnitToTeam(@PathVariable String teamName, @PathVariable String role, @PathVariable String unitName) {
        if (!checkTeamRole(role)) {
            throw new EnumNotFoundException(ErrorMessage.WRONG_ROLE_SPECIFIED_ERROR_MSG);
        }
        teamService.addGroupToTeam(teamName, role, unitName);
        return ResponseEntity.noContent().build();
    }

    @Operation(description = "Get a list of users for the collab, only returns users with the specified role", summary = "Get a list of users on the collab team that has the given role", responses = {
            @ApiResponse(responseCode = "200", description = API_SUCCESS_MSG, content = @Content(mediaType = "application/json", array = @ArraySchema(schema = @Schema(implementation = CollabUser.class)))),
            @ApiResponse(responseCode = "401", description = API_LOGIN_REQUIRED_ERROR_MSG),
            @ApiResponse(responseCode = "404", description = API_TEAM_NOT_FOUND_ERROR_MSG),
            @ApiResponse(responseCode = "406", description = WRONG_ROLE_SPECIFIED_ERROR_MSG)
    })
    @GetMapping("/{teamName}/{role}/users")
    public ResponseEntity<List<CollabUser>> getTeamMembershipUsersByRole(@PathVariable String teamName, @PathVariable String role) {
        if (!checkTeamRole(role)) {
            throw new EnumNotFoundException(ErrorMessage.WRONG_ROLE_SPECIFIED_ERROR_MSG);
        }
        return ResponseEntity.ok(teamService.getTeamMembershipUsersByRole(teamName, role));
    }

    @Operation(description = "Get groups in a team by the specified role", summary = "Get groups in a team by the specified role", responses = {
            @ApiResponse(responseCode = "200", description = API_SUCCESS_MSG, content = @Content(mediaType = "application/json", array = @ArraySchema(schema = @Schema(implementation = Group.class)))),
            @ApiResponse(responseCode = "401", description = API_LOGIN_REQUIRED_ERROR_MSG),
            @ApiResponse(responseCode = "404", description = API_TEAM_NOT_FOUND_ERROR_MSG),
            @ApiResponse(responseCode = "406", description = WRONG_ROLE_SPECIFIED_ERROR_MSG)
    })
    @GetMapping("/{teamName}/{role}/groups")
    public ResponseEntity<List<Group>> getTeamMembershipGroupsByRole(@PathVariable String teamName, @PathVariable String role) {
        if (!checkTeamRole(role)) {
            throw new EnumNotFoundException(ErrorMessage.WRONG_ROLE_SPECIFIED_ERROR_MSG);
        }
        return ResponseEntity.ok(teamService.getTeamMembershipGroupsByRole(teamName, role));
    }

    @Operation(description = "Get a list of units that belong to the collab's team", summary = "Get a list of units that belong to the collab's team", responses = {
            @ApiResponse(responseCode = "200", description = API_SUCCESS_MSG, content = @Content(mediaType = "application/json", array = @ArraySchema(schema = @Schema(implementation = Unit.class)))),
            @ApiResponse(responseCode = "401", description = API_LOGIN_REQUIRED_ERROR_MSG),
            @ApiResponse(responseCode = "404", description = API_TEAM_NOT_FOUND_ERROR_MSG),
            @ApiResponse(responseCode = "406", description = WRONG_ROLE_SPECIFIED_ERROR_MSG)
    })
    @GetMapping("/{teamName}/{role}/units")
    public ResponseEntity<List<Unit>> getTeamMembershipUnitsByRole(@PathVariable String teamName, @PathVariable String role) {
        if (!checkTeamRole(role)) {
            throw new EnumNotFoundException(ErrorMessage.WRONG_ROLE_SPECIFIED_ERROR_MSG);
        }
        return ResponseEntity.ok(teamService.getTeamMembershipUnitsByRole(teamName, role));
    }

    private void checkCreateTeamForm(CreateTeamForm form) throws InvalidParameterException {
        String teamName = form.getName();
        String teamDescription = form.getDescription();

        if (Strings.isNullOrEmpty(teamName)) {
            throw new InvalidParameterException(ErrorMessage.TEAM_NAME_EMPTY_ERROR_MSG);
        }

        if (Strings.isNullOrEmpty(teamDescription)) {
            throw new InvalidParameterException(ErrorMessage.TEAM_DESCRIPTION_EMPTY_ERROR_MSG);
        }

        if (teamName.length() < 5 || teamName.length() > 40) {
            throw new InvalidParameterException(ErrorMessage.TEAM_NAME_SHORT_OR_LONG_ERROR_MSG);
        }

        if (teamName.matches(".*[+#/\\;@<>\"].*")) {
            throw new InvalidParameterException(ErrorMessage.TEAM_NAME_WITH_SPECIAL_CHARS_ERROR_MSG);
        }
    }
}
