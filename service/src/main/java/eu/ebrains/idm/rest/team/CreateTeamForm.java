package eu.ebrains.idm.rest.team;

public class CreateTeamForm {

    private String name;
    private String description;

    public CreateTeamForm() {
        super();
    }

    public CreateTeamForm(String name, String description) {
        this.name = name;
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
