package eu.ebrains.idm.rest.group;

import eu.ebrains.idm.exception.AuthorizationException;
import eu.ebrains.idm.exception.EnumNotFoundException;
import eu.ebrains.idm.exception.ErrorMessage;
import eu.ebrains.idm.model.dto.CollabUser;
import eu.ebrains.idm.model.dto.Group;
import eu.ebrains.idm.model.dto.Membership;
import eu.ebrains.idm.service.impl.GroupServiceImpl;
import eu.ebrains.idm.utils.IDMUtils;
import eu.ebrains.idm.utils.enums.Role;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static eu.ebrains.idm.exception.ErrorMessage.*;
import static eu.ebrains.idm.utils.IDMUtils.checkGroupRole;

@Tag(name = "Group")
@RestController
@RequestMapping("/groups")
public class GroupResource {

    @Autowired
    private GroupServiceImpl groupService;


    @Operation(description = "Get a list of groups", summary = "Get a list of groups", responses = {
            @ApiResponse(responseCode = "200", description = API_SUCCESS_MSG, content = @Content(mediaType = "application/json", array = @ArraySchema(schema = @Schema(implementation = Group.class)))),
            @ApiResponse(responseCode = "401", description = API_LOGIN_REQUIRED_ERROR_MSG)
    })
    @GetMapping
    public ResponseEntity<List<Group>> getAllGroups() {
        return ResponseEntity.ok(groupService.getAllGroups());
    }

    @Operation(description = "Get a group by Id", summary = "Get a group by Id", responses = {
            @ApiResponse(responseCode = "200", description = API_SUCCESS_MSG, content = @Content(mediaType = "application/json", schema = @Schema(implementation = Group.class))),
            @ApiResponse(responseCode = "401", description = API_LOGIN_REQUIRED_ERROR_MSG),
            @ApiResponse(responseCode = "404", description = API_GROUP_NOT_FOUND_ERROR_MSG)
    })
    @GetMapping("/{groupName}")
    public ResponseEntity<Group> getGroupById(@PathVariable String groupName) {
        return ResponseEntity.ok(groupService.getGroupByName(groupName));
    }

    @Operation(description = "List the contents of the specified role in the specified Group", summary = "List the users, units and groups in the specified role and Group", responses = {
            @ApiResponse(responseCode = "200", description = API_SUCCESS_MSG, content = @Content(mediaType = "application/json", array = @ArraySchema(schema = @Schema(implementation = Membership.class)))),
            @ApiResponse(responseCode = "401", description = API_LOGIN_REQUIRED_ERROR_MSG),
            @ApiResponse(responseCode = "404", description = API_GROUP_NOT_FOUND_ERROR_MSG),
            @ApiResponse(responseCode = "406", description = WRONG_ROLE_SPECIFIED_ERROR_MSG)
    })
    @GetMapping("/{groupName}/{role}")
    public ResponseEntity<Membership> getGroupMembershipByRole(@PathVariable String groupName, @PathVariable String role) {
        if (!checkGroupRole(role)) {
            throw new EnumNotFoundException(ErrorMessage.WRONG_ROLE_SPECIFIED_ERROR_MSG);
        }
//        checkIfUserCanViewGroupMembers(IDMUtils.getUserLogged(), groupName);
        return ResponseEntity.ok(groupService.getGroupMembershipByRole(groupName,Role.fromKey(role)));
    }

    @Operation(description = "List the users contained in the specified role in the specified Group", summary = "List the users in the specified role and Group", responses = {
            @ApiResponse(responseCode = "200", description = API_SUCCESS_MSG, content = @Content(mediaType = "application/json", array = @ArraySchema(schema = @Schema(implementation = CollabUser.class)))),
            @ApiResponse(responseCode = "401", description = API_LOGIN_REQUIRED_ERROR_MSG),
            @ApiResponse(responseCode = "404", description = API_GROUP_NOT_FOUND_ERROR_MSG),
            @ApiResponse(responseCode = "406", description = WRONG_ROLE_SPECIFIED_ERROR_MSG)
    })
    @GetMapping("/{groupName}/{role}/users")
    public ResponseEntity<List<CollabUser>> getGroupMembershipUsersByRole(@PathVariable String groupName, @PathVariable String role) {
        if (!checkGroupRole(role)) {
            throw new EnumNotFoundException(ErrorMessage.WRONG_ROLE_SPECIFIED_ERROR_MSG);
        }
//        checkIfUserCanViewGroupMembers(IDMUtils.getUserLogged(), groupName);
        return ResponseEntity.ok(groupService.getGroupMembershipUsersByRole(groupName, role));
    }

    @Operation(description = "List the groups contained in the specified role in the specified Group", summary = "List the groups in the specified role and Group", responses = {
            @ApiResponse(responseCode = "200", description = API_SUCCESS_MSG, content = @Content(mediaType = "application/json", array = @ArraySchema(schema = @Schema(implementation = Group.class)))),
            @ApiResponse(responseCode = "401", description = API_LOGIN_REQUIRED_ERROR_MSG),
            @ApiResponse(responseCode = "404", description = API_GROUP_NOT_FOUND_ERROR_MSG),
            @ApiResponse(responseCode = "406", description = WRONG_ROLE_SPECIFIED_ERROR_MSG)
    })
    @GetMapping("/{groupName}/{role}/groups")
    public ResponseEntity<List<Group>> getGroupMembershipGroupsByRole(@PathVariable String groupName, @PathVariable String role) {
        if (!checkGroupRole(role)) {
            throw new EnumNotFoundException(ErrorMessage.WRONG_ROLE_SPECIFIED_ERROR_MSG);
        }
//        checkIfUserCanViewGroupMembers(IDMUtils.getUserLogged(), groupName);
        return ResponseEntity.ok(groupService.getGroupMembershipGroupsByRole(groupName, role));
    }

    @Operation(description = "Create a group", summary = "Create a group", responses = {
            @ApiResponse(responseCode = "201", description = API_CREATED_MSG, content = @Content(mediaType = "application/json", schema = @Schema(implementation = Group.class))),
            @ApiResponse(responseCode = "401", description = API_LOGIN_REQUIRED_ERROR_MSG),
            @ApiResponse(responseCode = "409", description = API_GROUP_ALREADY_EXISTS)
    })
    @PostMapping
    public ResponseEntity<Group> createGroup(@RequestBody Group group) {
        return ResponseEntity.status(HttpStatus.CREATED).body(groupService.createGroup(group));
    }

    @Operation(description = "Update a group", summary = "Update a group", responses = {
            @ApiResponse(responseCode = "201", description = API_CREATED_MSG),
            @ApiResponse(responseCode = "401", description = API_LOGIN_REQUIRED_ERROR_MSG),
            @ApiResponse(responseCode = "404", description = API_GROUP_NOT_FOUND_ERROR_MSG)
    })
    @PutMapping("/{groupName}")
    public ResponseEntity<Group> updateGroup(@PathVariable String groupName, @RequestBody Group group) {
//        if (groupService.isAdminOfGroup(IDMUtils.getUserLogged(), groupName)) {
//            throw new AuthorizationException(ErrorMessage.MUST_BE_A_GROUP_ADMIN_MSG);
//        }
        return ResponseEntity.status(HttpStatus.CREATED).body(groupService.updateGroup(groupName, group));
    }


    @Operation(description = "Delete a group", summary = "Delete a group", responses = {
            @ApiResponse(responseCode = "204", description = API_NO_CONTENT_MSG),
            @ApiResponse(responseCode = "401", description = API_LOGIN_REQUIRED_ERROR_MSG),
            @ApiResponse(responseCode = "404", description = API_GROUP_NOT_FOUND_ERROR_MSG)
    })
    @DeleteMapping("/{groupName}")
    public ResponseEntity deleteGroup(@PathVariable String groupName) {

//        if (groupService.isAdminOfGroup(IDMUtils.getUserLogged(), groupName)) {
//            throw new AuthorizationException(ErrorMessage.MUST_BE_A_GROUP_ADMIN_MSG);
//        }

        groupService.deleteGroup(groupName);
        return ResponseEntity.noContent().build();
    }

    @Operation(description = "Delete a user from group with the role specified", summary = "Delete a user from group with the role specified", responses = {
            @ApiResponse(responseCode = "200", description = API_SUCCESS_MSG),
            @ApiResponse(responseCode = "401", description = API_LOGIN_REQUIRED_ERROR_MSG),
            @ApiResponse(responseCode = "404", description = API_GROUP_NOT_FOUND_ERROR_MSG + "<br/>"
                    + API_USER_NOT_FOUND_ERROR_MSG + "<br/>"
                    + API_USER_NOT_MEMBER_OF_GROUP_ERROR_MSG),
            @ApiResponse(responseCode = "406", description = WRONG_ROLE_SPECIFIED_ERROR_MSG)
    })
    @DeleteMapping("/groups/{groupName}/{role}/users/{username}")
    public  ResponseEntity deleteUserFromGroup(@PathVariable String groupName, @PathVariable String role, @PathVariable String username) {
        if (!checkGroupRole(role)) {
            throw new EnumNotFoundException(ErrorMessage.WRONG_ROLE_SPECIFIED_ERROR_MSG);
        }
        groupService.deleteUserFromGroup(groupName, username, role);
        return ResponseEntity.ok().build();
    }

    @Operation(description = "Remove a group from the specified role in the specified group", summary = "Remove a group from this group", responses = {
            @ApiResponse(responseCode = "200", description = API_SUCCESS_MSG),
            @ApiResponse(responseCode = "401", description = API_LOGIN_REQUIRED_ERROR_MSG),
            @ApiResponse(responseCode = "404", description = API_GROUP_NOT_FOUND_ERROR_MSG + "<br/>"
                    + API_GROUP_NOT_MEMBER_OF_GROUP_ERROR_MSG),
            @ApiResponse(responseCode = "406", description = WRONG_ROLE_SPECIFIED_ERROR_MSG)
    })
    @DeleteMapping("/{groupName}/{role}/groups/{groupToUnassignName}")
    public  ResponseEntity deleteGroupFromGroup(@PathVariable String groupName, @PathVariable String role, @PathVariable String groupToUnassignName) {
        if (!checkGroupRole(role)) {
            throw new EnumNotFoundException(ErrorMessage.WRONG_ROLE_SPECIFIED_ERROR_MSG);
        }
        groupService.deleteGroupFromGroup(groupName, groupToUnassignName, role);
        return ResponseEntity.ok().build();
    }

    @Operation(description = "Remove a unit from the specified role in the specified group", summary = "Remove a unit from the group", responses = {
            @ApiResponse(responseCode = "200", description = API_SUCCESS_MSG),
            @ApiResponse(responseCode = "401", description = API_LOGIN_REQUIRED_ERROR_MSG),
            @ApiResponse(responseCode = "404", description = API_GROUP_NOT_FOUND_ERROR_MSG + "<br/>"
                    + API_UNIT_NOT_FOUND_ERROR_MSG + "<br/>"
                    + API_UNIT_NOT_MEMBER_OF_GROUP_ERROR_MSG),
            @ApiResponse(responseCode = "406", description = WRONG_ROLE_SPECIFIED_ERROR_MSG)
    })
    @DeleteMapping("/{groupName}/{role}/units/{unitName}")
    public  ResponseEntity deleteUnitFromGroup(@PathVariable String groupName, @PathVariable String role, @PathVariable String unitName) {
        if (!checkGroupRole(role)) {
            throw new EnumNotFoundException(ErrorMessage.WRONG_ROLE_SPECIFIED_ERROR_MSG);
        }
//        if (groupService.isAdminOfGroup(IDMUtils.getUserLogged(), groupName)) {
//            throw new AuthorizationException(ErrorMessage.MUST_BE_A_GROUP_ADMIN_MSG);
//        }
        groupService.deleteGroupFromGroup(groupName, unitName, role);
        return ResponseEntity.ok().build();
    }

    @Operation(description = "Add a user in the group with the role specified", summary = "Add a user in the given group", responses = {
            @ApiResponse(responseCode = "200", description = API_SUCCESS_MSG),
            @ApiResponse(responseCode = "401", description = API_LOGIN_REQUIRED_ERROR_MSG),
            @ApiResponse(responseCode = "404", description = API_GROUP_NOT_FOUND_ERROR_MSG + "<br/>"
                    + API_USER_NOT_FOUND_ERROR_MSG + "<br/>"
                    + API_USER_NOT_MEMBER_OF_GROUP_ERROR_MSG),
            @ApiResponse(responseCode = "406", description = WRONG_ROLE_SPECIFIED_ERROR_MSG)
    })
    @PutMapping("/{groupName}/{role}/users/{username}")
    public  ResponseEntity addUserToGroup(@PathVariable String groupName, @PathVariable String role, @PathVariable String username) {
        if (!checkGroupRole(role)) {
            throw new EnumNotFoundException(ErrorMessage.WRONG_ROLE_SPECIFIED_ERROR_MSG);
        }
        groupService.addUserToGroup(groupName, role, username);
        return ResponseEntity.ok().build();
    }

    @Operation(description = "Add a group to a group with the specified role", summary = "Add a group to a group with the specified role", responses = {
            @ApiResponse(responseCode = "204", description = API_NO_CONTENT_MSG),
            @ApiResponse(responseCode = "401", description = API_LOGIN_REQUIRED_ERROR_MSG),
            @ApiResponse(responseCode = "404", description = API_GROUP_NOT_FOUND_ERROR_MSG + "<br/>"
                    + API_GROUP_NOT_MEMBER_OF_GROUP_ERROR_MSG),
            @ApiResponse(responseCode = "406", description = WRONG_ROLE_SPECIFIED_ERROR_MSG)
    })
    @PutMapping("/{groupName}/{role}/groups/{childGroupName}")
    public  ResponseEntity addGroupToGroup(@PathVariable String groupName, @PathVariable String role, @PathVariable String childGroupName) {
        if (!checkGroupRole(role)) {
            throw new EnumNotFoundException(ErrorMessage.WRONG_ROLE_SPECIFIED_ERROR_MSG);
        }
        groupService.addGroupToGroup(groupName, role, childGroupName);
        return ResponseEntity.noContent().build();
    }

    @Operation(description = "Add a unit in the group with the role specified", summary = "Add a unit to the group", responses = {
            @ApiResponse(responseCode = "200", description = API_SUCCESS_MSG),
            @ApiResponse(responseCode = "401", description = API_LOGIN_REQUIRED_ERROR_MSG),
            @ApiResponse(responseCode = "404", description = API_GROUP_NOT_FOUND_ERROR_MSG + "<br/>"
                    + API_UNIT_NOT_FOUND_ERROR_MSG + "<br/>"
                    + API_UNIT_NOT_MEMBER_OF_GROUP_ERROR_MSG),
            @ApiResponse(responseCode = "406", description = WRONG_ROLE_SPECIFIED_ERROR_MSG)
    })
    @PutMapping("/{groupName}/{role}/units/{unitName}")
    public  ResponseEntity addUnitToGroup(@PathVariable String groupName, @PathVariable String role, @PathVariable String unitName) {
        if (!checkGroupRole(role)) {
            throw new EnumNotFoundException(ErrorMessage.WRONG_ROLE_SPECIFIED_ERROR_MSG);
        }
//        if (groupService.isAdminOfGroup(IDMUtils.getUserLogged(), groupName)) {
//            throw new AuthorizationException(ErrorMessage.MUST_BE_A_GROUP_ADMIN_MSG);
//        }
        groupService.addGroupToGroup(groupName, role, unitName);
        return ResponseEntity.ok().build();
    }

    private void checkIfUserCanViewGroupMembers(String username, String groupName) {
        if (!groupService.isUserMemberOrAdminOfGroup(username, groupName)) {
            throw new AuthorizationException(ErrorMessage.MUST_BE_A_GROUP_MEMBER_OR_ADMIN_MSG);
        }
    }
}
