package eu.ebrains.idm.rest.group;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import eu.ebrains.idm.model.dto.CollabUser;
import eu.ebrains.idm.model.dto.Unit;

import java.util.ArrayList;
import java.util.List;

/**
 * In our model, a Unit is based on the tree group hierarchy from Keycloak. We are actually using the keyword Group in
 * the project from the groups created ( as role ) with the IDM app.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class UnitDetail extends Unit
{
    private List<CollabUser> members = new ArrayList<CollabUser>();

    public UnitDetail(Unit unit, List<CollabUser> members)
    {
        super(unit);
        this.members = members;
    }

    public List<CollabUser> getMembers()
    {
        return members;
    }

    public void setMembers(List<CollabUser> members)
    {
        this.members = members;
    }

}
