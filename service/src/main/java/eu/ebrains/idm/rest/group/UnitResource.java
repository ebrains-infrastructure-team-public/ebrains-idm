package eu.ebrains.idm.rest.group;

import eu.ebrains.idm.exception.AuthorizationException;
import eu.ebrains.idm.exception.EnumNotFoundException;
import eu.ebrains.idm.exception.ErrorMessage;
import eu.ebrains.idm.model.dto.Membership;
import eu.ebrains.idm.model.dto.Unit;
import eu.ebrains.idm.service.GroupService;
import eu.ebrains.idm.service.UnitService;
import eu.ebrains.idm.utils.IDMUtils;
import eu.ebrains.idm.utils.enums.Role;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.ws.rs.DefaultValue;
import javax.ws.rs.PathParam;
import javax.ws.rs.QueryParam;
import java.util.List;

import static eu.ebrains.idm.exception.ErrorMessage.*;
import static eu.ebrains.idm.utils.IDMUtils.checkGroupRole;

@Tag(name = "Unit")
@RestController
@RequestMapping("/units")
public class UnitResource {

    @Autowired
    private GroupService groupService;
    @Autowired
    private UnitService unitService;

    @Operation(description = "Get units in a group by the specified role", summary = "Get units in a group by the specified role", responses = {
            @ApiResponse(responseCode = "200", description = API_SUCCESS_MSG, content = @Content(mediaType = "application/json", array = @ArraySchema(schema = @Schema(implementation = Unit.class)))),
            @ApiResponse(responseCode = "401", description = API_LOGIN_REQUIRED_ERROR_MSG),
            @ApiResponse(responseCode = "404", description = API_GROUP_NOT_FOUND_ERROR_MSG),
            @ApiResponse(responseCode = "406", description = WRONG_ROLE_SPECIFIED_ERROR_MSG)
    })
    @GetMapping("/{groupName}/role/{role}/units") // TODO: 20/12/2023 Should check the endpoint path if exact or not
    public ResponseEntity<List<Unit>> getTeamMembershipUnitsByRole(@PathVariable String groupName, @PathVariable String role) {
        if (!checkGroupRole(role)) {
            throw new EnumNotFoundException(ErrorMessage.WRONG_ROLE_SPECIFIED_ERROR_MSG);
        }
        return ResponseEntity.ok(groupService.getGroupMembershipUnitsByRole(groupName, role));
    }

    @Operation(description = "Remove a user from a unit", summary = "Remove a user from a unit", responses = {
            @ApiResponse(responseCode = "204", description = API_NO_CONTENT_MSG),
            @ApiResponse(responseCode = "401", description = API_LOGIN_REQUIRED_ERROR_MSG),
            @ApiResponse(responseCode = "404", description = API_UNIT_NOT_FOUND_ERROR_MSG + "<br/>"
                    + API_USER_NOT_FOUND_ERROR_MSG + "<br/>"
                    + API_USER_NOT_MEMBER_OF_UNIT_ERROR_MSG)
    })
    @DeleteMapping("/{unitName}/users/{username}")
    public  ResponseEntity deleteUserFromUnit(@PathVariable String unitName, @PathVariable String username) {

        if (groupService.isAdminOfGroup(IDMUtils.getUserLogged(), unitName)) {
            throw new AuthorizationException(ErrorMessage.MUST_BE_A_GROUP_ADMIN_MSG);
        }

        groupService.deleteUserFromGroup(unitName, username, Role.MEMBER.getKey());
        return ResponseEntity.noContent().build();
    }

    @Operation(description = "Add a user to a unit", summary = "Add a user to a unit", responses = {
            @ApiResponse(responseCode = "204", description = API_NO_CONTENT_MSG),
            @ApiResponse(responseCode = "401", description = API_LOGIN_REQUIRED_ERROR_MSG),
            @ApiResponse(responseCode = "404", description = API_UNIT_NOT_FOUND_ERROR_MSG + "<br/>"
                    + API_USER_NOT_FOUND_ERROR_MSG + "<br/>"
                    + API_USER_NOT_MEMBER_OF_UNIT_ERROR_MSG),
            @ApiResponse(responseCode = "406", description = WRONG_ROLE_SPECIFIED_ERROR_MSG)
    })
    @PutMapping("/{unitName}/users/{username}")
    public  ResponseEntity addUserToUnit(@PathVariable String unitName, @PathVariable String username) {

        if (groupService.isAdminOfGroup(username, unitName)) {
            throw new AuthorizationException(ErrorMessage.MUST_BE_A_GROUP_ADMIN_MSG);
        }

        groupService.addUserToGroup(unitName, Role.MEMBER.getKey(), username);
        return ResponseEntity.noContent().build();
    }

    @Operation(description = "Get the members directly assigned to a Unit. Only admin can use the flatList = True value", summary = "Get the members of a Unit", responses = {
            @ApiResponse(responseCode = "200", description = API_SUCCESS_MSG),
            @ApiResponse(responseCode = "401", description = API_LOGIN_REQUIRED_ERROR_MSG),
            @ApiResponse(responseCode = "404", description = API_UNIT_NOT_FOUND_ERROR_MSG)
    })
    @GetMapping("/{unitName}")
    public ResponseEntity<UnitDetail> getUnit(@PathParam("unitName") String unitName,
                                              @Parameter(name = "flatList", description = "if True, return users of this Unit and all its leaf-side units. Else return users only of this unit", in = ParameterIn.QUERY) @QueryParam("flatList") @DefaultValue("false") boolean flatList) {
        if(flatList) {
            // TODO: 31/12/2023 Check if user is an XWiki admin if flatlist is YES
            return ResponseEntity.ok(unitService.getUnitsWithFlatListMembersByPath(unitName));
        }
        else {
            return ResponseEntity.ok(unitService.getUnitsWithMembersByPath(unitName));
        }
    }

    @Operation(description = "Get the administrators of a Unit", summary = "Get the administrators of a unit", responses = {
            @ApiResponse(responseCode = "200", description = "success", content = @Content(mediaType = "application/json", schema = @Schema(implementation = Membership.class))),
            @ApiResponse(responseCode = "401", description = LOGIN_REQUIRED_MSG)})
    @GetMapping("/{unitName}/administrator")
    public ResponseEntity<Membership> getUnitAdministrators(@PathParam("unitName") String unitName) {
        return ResponseEntity.ok(unitService.getMembershipAdministratorForUnit(unitName));

    }
}
