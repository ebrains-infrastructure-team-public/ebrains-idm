/*
 * Copyright 2020 EPFL/Human Brain Project PCO
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package eu.ebrains.idm;

import eu.ebrains.idm.model.normalized.GroupRoleEntity;
import eu.ebrains.idm.model.normalized.TeamRoleEntity;
import eu.ebrains.idm.model.normalized.UserEntity;
import eu.ebrains.idm.repository.GroupRepository;
import eu.ebrains.idm.repository.GroupRoleRepository;
import eu.ebrains.idm.repository.TeamRoleRepository;
import eu.ebrains.idm.repository.UserRepository;
import eu.ebrains.idm.utils.enums.Role;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configurers.AbstractHttpConfigurer;
import org.springframework.security.config.annotation.web.configurers.HeadersConfigurer;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import java.util.Arrays;
import java.util.List;

import static eu.ebrains.idm.utils.ModelUtils.generateId;

@SpringBootApplication
@EnableConfigurationProperties
@EntityScan(basePackages = {"eu.ebrains.idm.model.normalized"})
@EnableJpaRepositories(basePackages = "eu.ebrains.idm.repository")
@ComponentScan(basePackages = {"eu.ebrains.idm.*"})
public class IDMApplication  {

    public static void main(String[] args) {
        SpringApplication.run(IDMApplication.class, args);
    }

    @Bean
    ApplicationRunner init(
            GroupRepository groupRepository,
            TeamRoleRepository teamRoleRepository,
            GroupRoleRepository groupRoleRepository,
            UserRepository userRepository
            ) {

        return args -> {
            /*
            Please uncomment the following lines one time if you need to generate data for tests
             */

//            List<TeamRoleEntity> teamRoleEntities = Arrays.asList(
//                    new TeamRoleEntity(null, Role.EDITOR),
//                    new TeamRoleEntity(null, Role.ADMINISTRATOR),
//                    new TeamRoleEntity(null, Role.VIEWER)
//            );
//            List<GroupRoleEntity> groupRoleEntities = Arrays.asList(
//                    new GroupRoleEntity(null, Role.MEMBER),
//                    new GroupRoleEntity(null, Role.ADMINISTRATOR)
//            );
//
//            teamRoleRepository.saveAll(teamRoleEntities);
//            groupRoleRepository.saveAll(groupRoleEntities);
//
//            // Create test users
//            List<UserEntity> userEntities = Arrays.asList(
//                    new UserEntity(generateId(), "test-user1", "test-user1@ebrains.eu", false),
//                    new UserEntity(generateId(), "test-user2", "test-user2@ebrains.eu", false),
//                    new UserEntity(generateId(), "test-user3", "test-user3@ebrains.eu", false)
//            );
//            userRepository.saveAll(userEntities);

        };
    }

}