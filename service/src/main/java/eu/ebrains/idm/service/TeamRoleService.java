package eu.ebrains.idm.service;

import eu.ebrains.idm.model.dto.TeamRole;
import eu.ebrains.idm.utils.enums.Role;

import java.util.List;

public interface TeamRoleService {
    List<TeamRole> getAllTeamRoles();
    TeamRole getTeamRoleById(String teamRoleId);
    TeamRole getTeamRoleByName(Role role);
    TeamRole createTeamRole(TeamRole teamRole);
    TeamRole updateTeamRole(String teamRoleId, TeamRole teamRole);
    void deleteTeamRole(String teamRoleId);
}