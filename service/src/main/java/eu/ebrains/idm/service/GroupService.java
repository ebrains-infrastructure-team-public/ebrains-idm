package eu.ebrains.idm.service;

import eu.ebrains.idm.model.dto.CollabUser;
import eu.ebrains.idm.model.dto.Group;
import eu.ebrains.idm.model.dto.Membership;
import eu.ebrains.idm.model.dto.Unit;
import eu.ebrains.idm.utils.enums.Role;

import java.util.List;

public interface GroupService {
    List<Group> getAllGroups();
    Group getGroupById(String groupId);

    Group getGroupByName(String groupName);

    Group createGroup(Group group);
    void addUserToGroup(String groupName, String role, String username);
    void addGroupToGroup(String groupName, String role, String childGroupName);
    Membership getGroupMembershipByRole(String groupName, Role role);
    Group updateGroup(String groupName, Group group);
    void deleteGroup(String groupId);
    void deleteUserFromGroup(String groupName, String username, String role);
    void deleteGroupFromGroup(String groupName, String groupNameToDelete, String role);
    List<CollabUser> getGroupMembershipUsersByRole(String groupName, String role);
    List<Group> getGroupMembershipGroupsByRole(String groupName, String role);
    List<Unit> getGroupMembershipUnitsByRole(String groupName, String role);
    public boolean isAdminOfGroup(String username, String groupName);
    public boolean isUserMemberOrAdminOfGroup(String username, String groupName);
}