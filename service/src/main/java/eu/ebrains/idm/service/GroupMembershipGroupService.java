package eu.ebrains.idm.service;

import eu.ebrains.idm.model.dto.GroupMembershipGroup;
import eu.ebrains.idm.model.dto.TeamMembershipGroup;
import eu.ebrains.idm.model.dto.TeamMembershipUser;
import eu.ebrains.idm.utils.enums.Role;

import java.util.List;

public interface GroupMembershipGroupService {
    List<GroupMembershipGroup> getAllGroupMembershipGroups();
    GroupMembershipGroup getGroupMembershipGroupById(String groupMembershipGroupId);
    GroupMembershipGroup createGroupMembershipGroup(GroupMembershipGroup groupMembershipGroup);
    GroupMembershipGroup updateGroupMembershipGroup(String groupMembershipGroupId, GroupMembershipGroup groupMembershipGroup);
    void deleteGroupMembershipGroup(String groupMembershipGroupId);
    List<GroupMembershipGroup> getGroupMembershipGroupByRole(String groupName, Role role);
}