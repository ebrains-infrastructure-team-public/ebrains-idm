package eu.ebrains.idm.service.impl;

import eu.ebrains.idm.mapper.GroupMembershipUserMapper;
import eu.ebrains.idm.mapper.GroupMembershipUserMapperImpl;
import eu.ebrains.idm.model.dto.GroupMembershipUser;
import eu.ebrains.idm.model.normalized.GroupMembershipUserEntity;
import eu.ebrains.idm.model.normalized.TeamMembershipUserEntity;
import eu.ebrains.idm.repository.GroupMembershipUserRepository;
import eu.ebrains.idm.service.GroupMembershipUserService;
import eu.ebrains.idm.utils.enums.Role;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class GroupMembershipUserServiceImpl implements GroupMembershipUserService {

    @Autowired
    private GroupMembershipUserRepository groupMembershipUserRepository;
    private GroupMembershipUserMapper groupMembershipUserMapper = new GroupMembershipUserMapperImpl();

    @Override
    public List<GroupMembershipUser> getAllGroupMembershipUsers() {
        List<GroupMembershipUserEntity> groupMembershipUsers = groupMembershipUserRepository.findAll();
        return groupMembershipUserMapper.groupMembershipUserEntitiesToGroupMembershipUsers(groupMembershipUsers);
    }

    @Override
    public GroupMembershipUser getGroupMembershipUserById(String groupMembershipUserId) {
        // TODO: 31/10/2023 Implement deleting with key object
        return null;
    }

    @Override
    public GroupMembershipUser createGroupMembershipUser(GroupMembershipUser groupMembershipUser) {
        GroupMembershipUserEntity groupMembershipUserToSave = groupMembershipUserMapper.groupMembershipUserToGroupMembershipUserEntity(groupMembershipUser);
        GroupMembershipUserEntity savedGroupMembershipUser = groupMembershipUserRepository.save(groupMembershipUserToSave);
        return groupMembershipUserMapper.groupMembershipUserEntityToGroupMembershipUser(savedGroupMembershipUser);
    }

    @Override
    public GroupMembershipUser updateGroupMembershipUser(String groupMembershipUserId, GroupMembershipUser groupMembershipUser) {
        return null;
    }

    @Override
    public void deleteGroupMembershipUser(String groupMembershipUserId) {
        // TODO: 31/10/2023 Implement deleting with key object
    }

    @Override
    public List<GroupMembershipUser> getGroupMembershipUserByRole(String groupName, Role role) {
        List<GroupMembershipUserEntity> groupUsers = groupMembershipUserRepository.findAllByGroupNameAndGroupRoleRole(groupName,role);
        return groupMembershipUserMapper.groupMembershipUserEntitiesToGroupMembershipUsers(groupUsers);
    }
}
