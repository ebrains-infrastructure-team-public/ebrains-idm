package eu.ebrains.idm.service;

import eu.ebrains.idm.model.dto.GroupMembershipUser;
import eu.ebrains.idm.model.dto.TeamMembershipUser;
import eu.ebrains.idm.utils.enums.Role;

import java.util.List;

public interface GroupMembershipUserService {
    List<GroupMembershipUser> getAllGroupMembershipUsers();
    GroupMembershipUser getGroupMembershipUserById(String groupMembershipUserId);
    GroupMembershipUser createGroupMembershipUser(GroupMembershipUser groupMembershipUser);
    GroupMembershipUser updateGroupMembershipUser(String groupMembershipUserId, GroupMembershipUser groupMembershipUser);
    void deleteGroupMembershipUser(String groupMembershipUserId);
    List<GroupMembershipUser> getGroupMembershipUserByRole(String groupName, Role role);
}