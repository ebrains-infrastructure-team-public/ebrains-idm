package eu.ebrains.idm.service.impl;

import eu.ebrains.idm.mapper.UserMapper;
import eu.ebrains.idm.model.dto.CollabUser;
import eu.ebrains.idm.model.dto.User;
import eu.ebrains.idm.model.normalized.UserEntity;
import eu.ebrains.idm.repository.UserRepository;
import eu.ebrains.idm.service.UserService;
import jakarta.persistence.EntityNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private UserMapper userMapper;

    @Override
    public List<User> getAllUsers() {
        List<UserEntity> users = userRepository.findAll();
        return userMapper.userEntitiesToUsers(users);
    }

    @Override
    public User getUserById(String userId) {
        Optional<UserEntity> user = userRepository.findById(userId);
        return user.map(userMapper::userEntityToUser).orElse(null);
    }

    @Override
    public User getUserByUsername(String username) throws EntityNotFoundException {
        Optional<UserEntity> user = userRepository.findFirstByUsername(username);
        if(user.isPresent()) return userMapper.userEntityToUser(user.get());

        throw new EntityNotFoundException(String.format("% user not found", username));
    }

    @Override
    public User createUser(User user) {
        UserEntity userToSave = userMapper.userToUserEntity(user);
        UserEntity savedUser = userRepository.save(userToSave);
        return userMapper.userEntityToUser(savedUser);
    }

    @Override
    public Optional<Authentication> getCurrentUserAuthentication() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

        if (authentication != null && authentication.isAuthenticated()) {
            return Optional.of(authentication);
        }

        return Optional.empty();
    }

    @Override
    public User updateUser(String userId, User user) {
        return null;
    }

    @Override
    public void deleteUser(String userId) {
        userRepository.deleteById(userId);
    }

    @Override
    public List<CollabUser> searchUser(String search, int limit) {
        return null;
    }
}
