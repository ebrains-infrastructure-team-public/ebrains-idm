package eu.ebrains.idm.service.impl;

import eu.ebrains.idm.exception.AlreadyExistsException;
import eu.ebrains.idm.exception.ErrorMessage;
import eu.ebrains.idm.exception.ResourceNotFoundException;
import eu.ebrains.idm.exception.RoleChangeException;
import eu.ebrains.idm.mapper.CollabUserMapper;
import eu.ebrains.idm.mapper.GroupMapper;
import eu.ebrains.idm.mapper.TeamMapper;
import eu.ebrains.idm.mapper.UserMapper;
import eu.ebrains.idm.model.dto.*;
import eu.ebrains.idm.model.normalized.*;
import eu.ebrains.idm.repository.*;
import eu.ebrains.idm.rest.team.CreateTeamForm;
import eu.ebrains.idm.rest.team.CreateTeamResponse;
import eu.ebrains.idm.service.TeamMembershipGroupService;
import eu.ebrains.idm.service.TeamMembershipUserService;
import eu.ebrains.idm.service.TeamService;
import eu.ebrains.idm.service.UserService;
import eu.ebrains.idm.utils.enums.GroupType;
import eu.ebrains.idm.utils.enums.Role;
import jakarta.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static eu.ebrains.idm.utils.IDMUtils.isValidTeamRoleChange;

@Service
public class TeamServiceImpl implements TeamService {

    @Autowired
    private TeamRepository teamRepository;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private GroupRepository groupRepository;
    @Autowired
    private TeamMembershipUserRepository teamMembershipUserRepository;
    @Autowired
    private TeamMembershipGroupRepository teamMembershipGroupRepository;
    @Autowired
    private TeamMembershipUserService teamMembershipUserService;
    @Autowired
    private TeamMembershipGroupService teamMembershipGroupService;
    @Autowired
    private TeamRoleRepository teamRoleRepository;
    @Autowired
    private UserService userService;
    @Autowired
    private GroupMapper groupMapper;
    @Autowired
    private TeamMapper teamMapper;
    @Autowired
    private UserMapper userMapper;
    @Autowired
    private CollabUserMapper collabUserMapper;

    @Override
    public List<Team> getAllTeams() {
        List<TeamEntity> teams = teamRepository.findAll();
        return teamMapper.teamEntitiesToTeams(teams);
    }

    @Override
    public Team getTeamById(String teamId) {
        Optional<TeamEntity> team = teamRepository.findById(teamId);
        return team.map(teamMapper::teamEntityToTeam).orElse(null);
    }

    @Override
    public Team getTeamByName(String teamName) {
        Optional<TeamEntity> team = teamRepository.findByNameIgnoreCase(teamName);
        return team.map(teamMapper::teamEntityToTeam).orElse(null);
    }

    @Override
    @Transactional
    public CreateTeamResponse createTeam(CreateTeamForm form) {

        Optional<Authentication> userAuthenticated = userService.getCurrentUserAuthentication();

        Optional<TeamEntity> checkTeam = teamRepository.findByNameIgnoreCase(form.getName().toLowerCase());
        if (checkTeam.isPresent()) {
            throw new AlreadyExistsException(ErrorMessage.TEAM_ALREADY_EXIST_ERROR_MSG);
        }
        // Create the team
        TeamEntity team = new TeamEntity();
        team.setName(form.getName().toLowerCase());
        team.setDescription(form.getDescription());
        team = teamRepository.save(team);

        if(userAuthenticated.isPresent()) {
            String username = userAuthenticated.get().getName();
            TeamMembershipUserEntity teamMembershipUserEntity = new TeamMembershipUserEntity(
                    userMapper.userToUserEntity(userService.getUserByUsername(username)),
                    team,
                    teamRoleRepository.findFirstByRole(Role.ADMINISTRATOR)
            );
            teamMembershipUserRepository.save(teamMembershipUserEntity);
        }

        CreateTeamResponse response = new CreateTeamResponse(
                team.getId(),
                team.getName(),
                team.getDescription()
        );

        return response;
    }

    @Override
    public Team updateTeam(String teamId, Team team) {
        return null;
    }

    @Override
    @Transactional
    public void deleteTeam(String teamName) {
        Optional<TeamEntity> team = teamRepository.findByNameIgnoreCase(teamName);
        if (team.isPresent()) {
            teamRepository.deleteById(team.get().getId());
        } else {
            throw new ResourceNotFoundException(String.format(ErrorMessage.TEAM_NOT_FOUND_ERROR_MSG, teamName));
        }
    }

    @Override
    public void deleteUserFromTeam(String teamName, String username, String role) {
        Optional<TeamEntity> team = teamRepository.findByNameIgnoreCase(teamName);
        if (!team.isPresent()) {
            throw new ResourceNotFoundException(String.format(ErrorMessage.TEAM_NOT_FOUND_ERROR_MSG, teamName));
        }
        Optional<UserEntity> user = userRepository.findFirstByUsername(username);
        if (!user.isPresent()) {
            throw new ResourceNotFoundException(String.format(ErrorMessage.USER_NOT_FOUND_ERROR_MSG, username));
        }
        Optional<TeamMembershipUserEntity> teamMembershipUser = teamMembershipUserRepository.findFirstByTeamNameAndUserUsername(teamName, username);
        if (!teamMembershipUser.isPresent()) {
            throw new ResourceNotFoundException(String.format(ErrorMessage.USER_NOT_MEMBER_OF_TEAM_ERROR_MSG, username, teamName));
        }
        if (!teamMembershipUser.get().getTeamRole().getRole().equals(Role.fromKey(role))) {
            throw new ResourceNotFoundException(String.format(ErrorMessage.USER_HAS_DIFFERENT_ROLE_OF_TEAM_ERROR_MSG, username, teamName));
        }
        teamMembershipUserRepository.deleteById(teamMembershipUser.get().getId());
        teamMembershipUserRepository.flush();
    }

    @Override
    public void deleteGroupFromTeam(String teamName, String groupName, String role) {
        Optional<TeamEntity> team = teamRepository.findByNameIgnoreCase(teamName);
        if (!team.isPresent()) {
            throw new ResourceNotFoundException(String.format(ErrorMessage.TEAM_NOT_FOUND_ERROR_MSG, teamName));
        }
        Optional<GroupEntity> group = groupRepository.findByNameIgnoreCase(groupName);
        if (!group.isPresent()) {
            throw new ResourceNotFoundException(String.format(ErrorMessage.GROUP_NOT_FOUND_ERROR_MSG, groupName));
        }
        Optional<TeamMembershipGroupEntity> teamMembershipGroup = teamMembershipGroupRepository.findFirstByTeamNameIgnoreCaseAndGroupNameIgnoreCase(teamName, groupName);
        if (!teamMembershipGroup.isPresent()) {
            throw new ResourceNotFoundException(String.format(ErrorMessage.GROUP_NOT_MEMBER_OF_TEAM_ERROR_MSG, groupName, teamName));
        }
        if (!teamMembershipGroup.get().getTeamRole().getRole().equals(Role.fromKey(role))) {
            throw new ResourceNotFoundException(String.format(ErrorMessage.GROUP_HAS_DIFFERENT_ROLE_OF_TEAM_ERROR_MSG, groupName, teamName));
        }
        teamMembershipGroupRepository.deleteById(teamMembershipGroup.get().getId());
    }

    public List<CollabUser> getUsersInTeam(String teamName, Role role) {
        List<TeamMembershipUser> usersInTeam = teamMembershipUserService.getTeamMembershipUserByRole(teamName,role);

        return usersInTeam
                .stream()
                .map(teamMembershipUser -> collabUserMapper.userToCollabUser(teamMembershipUser.getUser()))
                .collect(Collectors.toList());
    }

    private List<Group> getGroupsInTeam(String teamName, Role role) {
        List<TeamMembershipGroup> groupsInTeam = teamMembershipGroupService.getTeamMembershipGroupByRole(teamName,role);

        return groupsInTeam
                .stream()
                .filter(teamMembershipGroup -> GroupType.GROUP.equals(teamMembershipGroup.getGroup().getType()))
                .map(teamMembershipGroup -> teamMembershipGroup.getGroup())
                .collect(Collectors.toList());
    }

    private List<Unit> getUnitsInTeam(String teamName, Role role) {
        List<TeamMembershipGroup> groupsInTeam = teamMembershipGroupService.getTeamMembershipGroupByRole(teamName,role);

        return groupsInTeam
                .stream()
                .filter(teamMembershipGroup -> GroupType.UNIT.equals(teamMembershipGroup.getGroup().getType()))
                .map(teamMembershipGroup -> groupMapper.groupToUnit(teamMembershipGroup.getGroup()))
                .collect(Collectors.toList());
    }

    @Override
    public Membership getTeamMembershipByRole(String teamName, Role role) {
        Membership membership = new Membership();

        List<CollabUser> userList = getUsersInTeam(teamName,role);
        membership.setUsers(userList);

        List<Group> groupsInTeam = getGroupsInTeam(teamName,role);
        membership.setGroups(groupsInTeam);

        List<Unit> unitsInTeam = getUnitsInTeam(teamName,role);
        membership.setUnits(unitsInTeam);

        return membership;
    }

    @Override
    @Transactional
    public void addUserToTeam(String teamName, String role, String username) {
        Optional<UserEntity> user = userRepository.findFirstByUsername(username);

        if (!user.isPresent()) {
            throw new ResourceNotFoundException(String.format(ErrorMessage.USER_NOT_FOUND_ERROR_MSG, username));
        }

        Optional<TeamEntity> checkTeam = teamRepository.findByNameIgnoreCase(teamName);
        if (!checkTeam.isPresent()) {
            throw new ResourceNotFoundException(String.format(ErrorMessage.TEAM_NOT_FOUND_ERROR_MSG, teamName));
        }

        TeamEntity team = checkTeam.get();

        Optional<TeamMembershipUserEntity> checkMembership = teamMembershipUserRepository.findFirstByTeamNameAndUserUsername(teamName, username);

        if (checkMembership.isPresent()) {
            Boolean isValidRoleChange = isValidTeamRoleChange(checkMembership.get().getTeamRole().getRole(), Role.fromKey(role.toLowerCase()));
            if (isValidRoleChange != null) {
                if (isValidRoleChange) {
                    teamMembershipUserRepository.deleteById(checkMembership.get().getId());
                    teamMembershipUserRepository.flush();
                } else {
                    throw new RoleChangeException(ErrorMessage.USER_LOWER_ROLE_CHANGE_ERROR_MSG);
                }
            } else {
                throw new RoleChangeException(ErrorMessage.USER_SAME_ROLE_CHANGE_ERROR_MSG);
            }
        }

        TeamMembershipUserEntity teamMembershipUser = new TeamMembershipUserEntity(
                user.get(),
                team,
                teamRoleRepository.findFirstByRole(Role.fromKey(role.toLowerCase()))
        );
        teamMembershipUserRepository.save(teamMembershipUser);
    }

    @Override
    @Transactional
    public void addGroupToTeam(String teamName, String role, String groupName) {
        Optional<GroupEntity> group = groupRepository.findByNameIgnoreCase(groupName);
        if (!group.isPresent()) {
            throw new ResourceNotFoundException(String.format(ErrorMessage.GROUP_NOT_FOUND_ERROR_MSG, groupName));
        }

        Optional<TeamEntity> checkTeam = teamRepository.findByNameIgnoreCase(teamName);
        if (!checkTeam.isPresent()) {
            throw new ResourceNotFoundException(String.format(ErrorMessage.TEAM_NOT_FOUND_ERROR_MSG, teamName));
        }

        TeamEntity team = checkTeam.get();

        Optional<TeamMembershipGroupEntity> checkMembership = teamMembershipGroupRepository.findFirstByTeamNameIgnoreCaseAndGroupNameIgnoreCase(teamName, groupName);

        if (checkMembership.isPresent()) {
            Boolean isValidRoleChange = isValidTeamRoleChange(checkMembership.get().getTeamRole().getRole(), Role.fromKey(role.toLowerCase()));

            if (isValidRoleChange != null) {
                if (isValidRoleChange) {
                    teamMembershipGroupRepository.deleteById(checkMembership.get().getId());
                    teamMembershipGroupRepository.flush();
                } else {
                    throw new RoleChangeException(ErrorMessage.GROUP_LOWER_ROLE_CHANGE_ERROR_MSG);
                }
            } else {
                throw new RoleChangeException(ErrorMessage.GROUP_SAME_ROLE_CHANGE_ERROR_MSG);
            }
        }

        TeamMembershipGroupEntity teamMembershipGroup = new TeamMembershipGroupEntity(
                group.get(),
                team,
                teamRoleRepository.findFirstByRole(Role.fromKey(role.toLowerCase()))
        );
        teamMembershipGroupRepository.save(teamMembershipGroup);

    }

    @Override
    public List<CollabUser> getTeamMembershipUsersByRole(String teamName, String role) {
        Team team = this.getTeamByName(teamName);
        if (team == null) {
            throw new ResourceNotFoundException(String.format(ErrorMessage.TEAM_NOT_FOUND_ERROR_MSG, teamName));
        }
        return this.getUsersInTeam(teamName, Role.fromKey(role));
    }

    @Override
    public List<Group> getTeamMembershipGroupsByRole(String teamName, String role) {
        Team team = this.getTeamByName(teamName);
        if (team == null) {
            throw new ResourceNotFoundException(String.format(ErrorMessage.TEAM_NOT_FOUND_ERROR_MSG, teamName));
        }
        return this.getGroupsInTeam(teamName, Role.fromKey(role));
    }

    @Override
    public List<Unit> getTeamMembershipUnitsByRole(String teamName, String role) {
        Team team = this.getTeamByName(teamName);
        if (team == null) {
            throw new ResourceNotFoundException(String.format(ErrorMessage.TEAM_NOT_FOUND_ERROR_MSG, teamName));
        }
        return this.getUnitsInTeam(teamName, Role.fromKey(role));
    }
}