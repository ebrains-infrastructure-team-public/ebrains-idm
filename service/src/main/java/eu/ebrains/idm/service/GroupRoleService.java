package eu.ebrains.idm.service;

import eu.ebrains.idm.model.dto.GroupRole;
import eu.ebrains.idm.utils.enums.Role;

import java.util.List;

public interface GroupRoleService {
    List<GroupRole> getAllGroupRoles();
    GroupRole getGroupRoleById(String groupRoleId);
    GroupRole getGroupRoleByName(Role role);
    GroupRole createGroupRole(GroupRole groupRole);
    void deleteGroupRole(String groupRoleId);
}