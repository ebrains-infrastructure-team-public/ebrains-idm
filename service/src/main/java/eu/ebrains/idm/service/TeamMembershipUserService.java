package eu.ebrains.idm.service;

import eu.ebrains.idm.model.dto.TeamMembershipUser;
import eu.ebrains.idm.utils.enums.Role;

import java.util.List;

public interface TeamMembershipUserService {
    List<TeamMembershipUser> getAllTeamMembershipUsers();
    TeamMembershipUser getTeamMembershipUserById(String teamMembershipUserId);
    TeamMembershipUser createTeamMembershipUser(TeamMembershipUser teamMembershipUser);
    TeamMembershipUser updateTeamMembershipUser(String teamMembershipUserId, TeamMembershipUser teamMembershipUser);
    void deleteTeamMembershipUser(String teamMembershipUserId);
    List<TeamMembershipUser> getTeamMembershipUserByRole(String teamName, Role role);
    TeamMembershipUser getTeamMembershipUserByTeamIdAndUserId(String teamId, String userId);
    TeamMembershipUser getTeamMembershipUserByTeamNameAndUserUsername(String teamName, String id);
}