package eu.ebrains.idm.service.impl;

import eu.ebrains.idm.exception.AlreadyExistsException;
import eu.ebrains.idm.exception.ErrorMessage;
import eu.ebrains.idm.exception.ResourceNotFoundException;
import eu.ebrains.idm.exception.RoleChangeException;
import eu.ebrains.idm.mapper.*;
import eu.ebrains.idm.model.dto.*;
import eu.ebrains.idm.model.normalized.GroupEntity;
import eu.ebrains.idm.model.normalized.GroupMembershipGroupEntity;
import eu.ebrains.idm.model.normalized.GroupMembershipUserEntity;
import eu.ebrains.idm.model.normalized.UserEntity;
import eu.ebrains.idm.repository.*;
import eu.ebrains.idm.service.*;
import eu.ebrains.idm.utils.enums.GroupType;
import eu.ebrains.idm.utils.enums.Role;
import jakarta.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static eu.ebrains.idm.utils.IDMUtils.isValidGroupRoleChange;

@Service
public class GroupServiceImpl implements GroupService {

    @Autowired
    private GroupRepository groupRepository;
    @Autowired
    private UserService userService;
    @Autowired
    private GroupRoleService groupRoleService;
    @Autowired
    private UserRepository userRepository;

    @Autowired
    private GroupRoleRepository groupRoleRepository;
    @Autowired
    private GroupMapper groupMapper;
    @Autowired
    private GroupRoleMapper groupRoleMapper;
    @Autowired
    private CollabUserMapper collabUserMapper;
    @Autowired
    private GroupMembershipGroupService groupMembershipGroupService;
    @Autowired
    private GroupMembershipUserService groupMembershipUserService;
    @Autowired
    private GroupMembershipUserRepository groupMembershipUserRepository;
    @Autowired
    private GroupMembershipGroupRepository groupMembershipGroupRepository;
    @Autowired
    private GroupMembershipUserMapper groupMembershipUserMapper;
    @Autowired
    private GroupMembershipGroupMapper groupMembershipGroupMapper;


    @Override
    public List<Group> getAllGroups() {
        List<GroupEntity> groups = groupRepository.findAll();
        return groupMapper.groupEntitiesToGroups(groups);
    }

    @Override
    public Group getGroupById(String groupId) {
        Optional<GroupEntity> group = groupRepository.findById(groupId);
        return group.map(groupMapper::groupEntityToGroup).orElse(null);
    }

    @Override
    public Group getGroupByName(String groupName) {
        Optional<GroupEntity> group = groupRepository.findByNameIgnoreCase(groupName);
        return group.map(groupMapper::groupEntityToGroup).orElse(null);
    }

    @Override
    @Transactional
    public Group createGroup(Group form) {

        Optional<Authentication> userAuthenticated = userService.getCurrentUserAuthentication();

        Optional<GroupEntity> checkGroup = groupRepository.findByNameIgnoreCase(form.getName().toLowerCase());
        if (checkGroup.isPresent()) {
            throw new AlreadyExistsException("Another group with the same name already exists");
        }
        // Create the group
        GroupEntity group = new GroupEntity();
        group.setName(form.getName().toLowerCase());
        group.setTitle(form.getTitle());
        group.setDescription(form.getDescription());
        group.setType(form.getType());
        group = groupRepository.save(group);

        if(userAuthenticated.isPresent()) {
            String username = userAuthenticated.get().getName();
            Optional<UserEntity> user = userRepository.findFirstByUsername(username);

            if(user.isPresent()) {
                // Make the logged user Admin of this group
                GroupMembershipUserEntity groupMembershipUserEntity = new GroupMembershipUserEntity(group,
                        user.get(),
                        groupRoleMapper.groupRoleToGroupRoleEntity(groupRoleService.getGroupRoleByName(Role.ADMINISTRATOR))
                );
            }
        }

        return form;
    }

    @Override
    @Transactional
    public void addUserToGroup(String groupName, String role, String username) {
        Optional<UserEntity> user = userRepository.findFirstByUsername(username);

        if (!user.isPresent()) {
            throw new ResourceNotFoundException(String.format(ErrorMessage.USER_NOT_FOUND_ERROR_MSG, username));
        }

        Optional<GroupEntity> checkGroup = groupRepository.findByNameIgnoreCase(groupName);
        if (!checkGroup.isPresent()) {
            throw new AlreadyExistsException(String.format(ErrorMessage.GROUP_NOT_FOUND_ERROR_MSG, groupName));
        }

        GroupEntity group = checkGroup.get();

        Optional<GroupMembershipUserEntity> checkMembership = groupMembershipUserRepository.findFirstByGroupNameAndUserUsername(groupName, username);

        if (checkMembership.isPresent()) {
            Boolean isValidRoleChange = isValidGroupRoleChange(checkMembership.get().getGroupRole().getRole(), Role.fromKey(role.toLowerCase()));
            if (isValidRoleChange != null) {
                if (isValidRoleChange) {
                    groupMembershipUserRepository.deleteById(checkMembership.get().getId());
                    groupMembershipUserRepository.flush();
                } else {
                    throw new RoleChangeException(ErrorMessage.USER_LOWER_ROLE_CHANGE_ERROR_MSG);
                }
            } else {
                throw new RoleChangeException(ErrorMessage.USER_SAME_ROLE_CHANGE_ERROR_MSG);
            }
        }

        GroupMembershipUserEntity groupMembershipUserEntity = new GroupMembershipUserEntity(
                group,
                user.get(),
                groupRoleRepository.findFirstByRole(Role.fromKey(role.toLowerCase()))
        );

        groupMembershipUserRepository.save(groupMembershipUserEntity);

    }

    @Override
    @Transactional
    public void addGroupToGroup(String groupName, String role, String childGroupName) {
        Optional<GroupEntity> checkChildGroup = groupRepository.findByNameIgnoreCase(childGroupName);
        if (!checkChildGroup.isPresent()) {
            throw new ResourceNotFoundException(String.format(ErrorMessage.GROUP_NOT_FOUND_ERROR_MSG, childGroupName));
        }

        Optional<GroupEntity> checkGroup = groupRepository.findByNameIgnoreCase(groupName);
        if (!checkGroup.isPresent()) {
            throw new AlreadyExistsException(String.format(ErrorMessage.GROUP_NOT_FOUND_ERROR_MSG, groupName));
        }

        GroupEntity group = checkGroup.get();

        Optional<GroupMembershipGroupEntity> checkMembership = groupMembershipGroupRepository.findFirstByParentGroupNameAndChildGroupName(groupName, childGroupName);

        if (checkMembership.isPresent()) {
            Boolean isValidRoleChange = isValidGroupRoleChange(checkMembership.get().getGroupRole().getRole(), Role.fromKey(role.toLowerCase()));

            if (isValidRoleChange != null) {
                if (isValidRoleChange) {
                    groupMembershipGroupRepository.deleteById(checkMembership.get().getId());
                    groupMembershipGroupRepository.flush();
                } else {
                    throw new RoleChangeException(ErrorMessage.GROUP_LOWER_ROLE_CHANGE_ERROR_MSG);
                }
            } else {
                throw new RoleChangeException(ErrorMessage.GROUP_SAME_ROLE_CHANGE_ERROR_MSG);
            }
        }

        GroupMembershipGroupEntity groupMembershipGroupEntity = new GroupMembershipGroupEntity(group,
                checkChildGroup.get(),
                groupRoleRepository.findFirstByRole(Role.fromKey(role.toLowerCase()))
        );

        groupMembershipGroupRepository.save(groupMembershipGroupEntity);

    }

    @Override
    public Membership getGroupMembershipByRole(String teamName, Role role) {
        Membership membership = new Membership();

        List<CollabUser> userList = getUsersInGroup(teamName,role);
        membership.setUsers(userList);

        List<Group> groupsInTeam = getGroupsInGroup(teamName,role);
        membership.setGroups(groupsInTeam);

        List<Unit> unitsInTeam = getUnitsInGroup(teamName,role);
        membership.setUnits(unitsInTeam);

        return membership;
    }

    public List<CollabUser> getUsersInGroup(String groupName, Role role) {
        List<GroupMembershipUser> usersInTeam = groupMembershipUserService.getGroupMembershipUserByRole(groupName,role);

        return usersInTeam
                .stream()
                .map(teamMembershipUser -> collabUserMapper.userToCollabUser(teamMembershipUser.getUser()))
                .collect(Collectors.toList());
    }

    private List<Group> getGroupsInGroup(String groupName, Role role) {
        List<GroupMembershipGroup> groupsInTeam = groupMembershipGroupService.getGroupMembershipGroupByRole(groupName,role);

        return groupsInTeam
                .stream()
                .filter(teamMembershipGroup -> GroupType.GROUP.equals(teamMembershipGroup.getChildGroup().getType()))
                .map(teamMembershipGroup -> teamMembershipGroup.getChildGroup())
                .collect(Collectors.toList());
    }

    private List<Unit> getUnitsInGroup(String groupName, Role role) {
        List<GroupMembershipGroup> groupsInTeam = groupMembershipGroupService.getGroupMembershipGroupByRole(groupName,role);

        return groupsInTeam
                .stream()
                .filter(groupMembershipGroup -> GroupType.UNIT.equals(groupMembershipGroup.getChildGroup().getType()))
                .map(groupMembershipGroup -> groupMapper.groupToUnit(groupMembershipGroup.getChildGroup()))
                .collect(Collectors.toList());
    }

    @Override
    public Group updateGroup(String groupName, Group group) {
        Optional<GroupEntity> checkGroup = groupRepository.findByNameIgnoreCase(groupName);
        if (checkGroup.isPresent()) {
            GroupEntity existedGroup = checkGroup.get();

            // Update only if the provided group's fields are not null
            if (group.getName() != null) {
                existedGroup.setName(group.getName());
            }
            if (group.getDescription() != null) {
                existedGroup.setDescription(group.getDescription());
            }
            if (group.getTitle() != null) {
                existedGroup.setTitle(group.getTitle());
            }

            GroupEntity updatedGroup = groupRepository.save(existedGroup);

            return groupMapper.groupEntityToGroup(updatedGroup);
        } else {
            throw new ResourceNotFoundException(String.format(ErrorMessage.GROUP_NOT_FOUND_ERROR_MSG, groupName));
        }
    }

    @Override
    public void deleteGroup(String groupName) {
        Optional<GroupEntity> group = groupRepository.findByNameIgnoreCase(groupName);
        if (group.isPresent()) {
            groupRepository.deleteById(group.get().getId());
        } else {
            throw new ResourceNotFoundException(String.format(ErrorMessage.GROUP_NOT_FOUND_ERROR_MSG, groupName));
        }
    }

    @Override
    public void deleteUserFromGroup(String groupName, String username, String role) {
        Optional<GroupEntity> group = groupRepository.findByNameIgnoreCase(groupName);
        if (!group.isPresent()) {
            throw new ResourceNotFoundException(String.format(ErrorMessage.GROUP_NOT_FOUND_ERROR_MSG, groupName));
        }
        Optional<UserEntity> user = userRepository.findFirstByUsername(username);
        if (!user.isPresent()) {
            throw new ResourceNotFoundException(String.format(ErrorMessage.USER_NOT_FOUND_ERROR_MSG, username));
        }
        Optional<GroupMembershipUserEntity> groupMembershipUser = groupMembershipUserRepository.findFirstByGroupNameAndUserUsername(groupName, username);
        if (!groupMembershipUser.isPresent()) {
            throw new ResourceNotFoundException(String.format(ErrorMessage.USER_NOT_MEMBER_OF_GROUP_ERROR_MSG, username, groupName));
        }
        if (!groupMembershipUser.get().getGroupRole().getRole().equals(Role.fromKey(role))) {
            throw new ResourceNotFoundException(String.format(ErrorMessage.USER_HAS_DIFFERENT_ROLE_OF_GROUP_ERROR_MSG, username, groupName));
        }
        groupMembershipUserRepository.deleteById(groupMembershipUser.get().getId());
        groupMembershipUserRepository.flush();
    }

    @Override
    public void deleteGroupFromGroup(String parentGroupName, String childGroupName, String role) {
        Optional<GroupEntity> parentGroup = groupRepository.findByNameIgnoreCase(parentGroupName);
        if (!parentGroup.isPresent()) {
            throw new ResourceNotFoundException(String.format(ErrorMessage.GROUP_NOT_FOUND_ERROR_MSG, parentGroupName));
        }
        Optional<GroupEntity> childGroup = groupRepository.findByNameIgnoreCase(childGroupName);
        if (!childGroup.isPresent()) {
            throw new ResourceNotFoundException(String.format(ErrorMessage.GROUP_NOT_FOUND_ERROR_MSG, childGroupName));
        }
        Optional<GroupMembershipGroupEntity> groupMembershipGroup = groupMembershipGroupRepository.findFirstByParentGroupNameAndChildGroupName(parentGroupName, childGroupName);
        if (!groupMembershipGroup.isPresent()) {
            throw new ResourceNotFoundException(String.format(ErrorMessage.GROUP_NOT_MEMBER_OF_GROUP_ERROR_MSG, parentGroupName, childGroupName));
        }
        if (!groupMembershipGroup.get().getGroupRole().getRole().equals(Role.fromKey(role))) {
            throw new ResourceNotFoundException(String.format(ErrorMessage.GROUP_HAS_DIFFERENT_ROLE_OF_GROUP_ERROR_MSG, parentGroupName, childGroupName));
        }
        groupMembershipGroupRepository.deleteById(groupMembershipGroup.get().getId());
    }

    @Override
    public List<CollabUser> getGroupMembershipUsersByRole(String groupName, String role) {
        Group group = this.getGroupByName(groupName);
        if (group == null) {
            throw new ResourceNotFoundException(String.format(ErrorMessage.GROUP_NOT_FOUND_ERROR_MSG, groupName));
        }
        return this.getUsersInGroup(groupName, Role.fromKey(role));
    }

    @Override
    public List<Group> getGroupMembershipGroupsByRole(String groupName, String role) {
        Group group = this.getGroupByName(groupName);
        if (group == null) {
            throw new ResourceNotFoundException(String.format(ErrorMessage.GROUP_NOT_FOUND_ERROR_MSG, groupName));
        }
        return this.getGroupsInGroup(groupName, Role.fromKey(role));
    }

    @Override
    public List<Unit> getGroupMembershipUnitsByRole(String groupName, String role) {
        Group group = this.getGroupByName(groupName);
        if (group == null) {
            throw new ResourceNotFoundException(String.format(ErrorMessage.GROUP_NOT_FOUND_ERROR_MSG, groupName));
        }
        return this.getUnitsInGroup(groupName, Role.fromKey(role));
    }

    @Override
    public boolean isAdminOfGroup(String username, String groupName) {
        Optional<UserEntity> checkUser = userRepository.findFirstByUsername(username);

        if (!checkUser.isPresent()) {
            throw new ResourceNotFoundException(String.format(ErrorMessage.USER_NOT_FOUND_ERROR_MSG, username));
        }

        Optional<GroupEntity> checkGroup = groupRepository.findByNameIgnoreCase(groupName);
        if (!checkGroup.isPresent()) {
            throw new AlreadyExistsException(String.format(ErrorMessage.GROUP_NOT_FOUND_ERROR_MSG, groupName));
        }

        Optional<GroupMembershipUserEntity> checkMembership = groupMembershipUserRepository.findFirstByGroupNameAndUserUsername(groupName, username);
        if (!checkMembership.isPresent()) {
            throw new ResourceNotFoundException(String.format(ErrorMessage.USER_NOT_MEMBER_OF_GROUP_ERROR_MSG, username, groupName));
        }
        if (checkMembership.get().getGroupRole().getRole().equals(Role.ADMINISTRATOR)) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public boolean isUserMemberOrAdminOfGroup(String username, String groupName) {
        Optional<UserEntity> checkUser = userRepository.findFirstByUsername(username);

        if (!checkUser.isPresent()) {
            throw new ResourceNotFoundException(String.format(ErrorMessage.USER_NOT_FOUND_ERROR_MSG, username));
        }

        Optional<GroupEntity> checkGroup = groupRepository.findByNameIgnoreCase(groupName);
        if (!checkGroup.isPresent()) {
            throw new AlreadyExistsException(String.format(ErrorMessage.GROUP_NOT_FOUND_ERROR_MSG, groupName));
        }

        Optional<GroupMembershipUserEntity> checkMembership = groupMembershipUserRepository.findFirstByGroupNameAndUserUsername(groupName, username);
        if (checkMembership.isPresent()) {
            return true;
        } else {
            return false;
        }
    }
}
