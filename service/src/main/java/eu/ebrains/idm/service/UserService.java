package eu.ebrains.idm.service;

import eu.ebrains.idm.model.dto.CollabUser;
import eu.ebrains.idm.model.dto.User;
import jakarta.persistence.EntityNotFoundException;
import org.springframework.security.core.Authentication;

import java.util.List;
import java.util.Optional;

public interface UserService {
    List<User> getAllUsers();
    User getUserById(String userId);
    User getUserByUsername(String username) throws EntityNotFoundException;
    User createUser(User user);
    Optional<Authentication> getCurrentUserAuthentication();
    User updateUser(String userId, User user);
    void deleteUser(String userId);
    List<CollabUser> searchUser(String search, int limit);
}