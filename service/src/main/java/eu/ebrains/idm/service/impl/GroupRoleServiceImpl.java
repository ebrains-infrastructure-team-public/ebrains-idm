package eu.ebrains.idm.service.impl;

import eu.ebrains.idm.mapper.GroupRoleMapper;
import eu.ebrains.idm.mapper.GroupRoleMapperImpl;
import eu.ebrains.idm.model.dto.GroupRole;
import eu.ebrains.idm.model.normalized.GroupRoleEntity;
import eu.ebrains.idm.repository.GroupRoleRepository;
import eu.ebrains.idm.service.GroupRoleService;
import eu.ebrains.idm.utils.enums.Role;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class GroupRoleServiceImpl implements GroupRoleService {

    @Autowired
    private GroupRoleRepository groupRoleRepository;
    private GroupRoleMapper groupRoleMapper = new GroupRoleMapperImpl();

    @Override
    public List<GroupRole> getAllGroupRoles() {
        List<GroupRoleEntity> groupRoles = groupRoleRepository.findAll();
        return groupRoleMapper.groupRoleEntitiesToGroupRoles(groupRoles);
    }

    @Override
    public GroupRole getGroupRoleById(String groupRoleId) {
        // TODO: 31/10/2023 Implement deleting with key object
        return null;
    }

    @Override
    public GroupRole getGroupRoleByName(Role role) {
        GroupRoleEntity groupRole = groupRoleRepository.findFirstByRole(role);
        return groupRoleMapper.groupRoleEntityToGroupRole(groupRole);
    }

    @Override
    public GroupRole createGroupRole(GroupRole groupRole) {
        GroupRoleEntity groupRoleToSave = groupRoleMapper.groupRoleToGroupRoleEntity(groupRole);
        GroupRoleEntity savedGroupRole = groupRoleRepository.save(groupRoleToSave);
        return groupRoleMapper.groupRoleEntityToGroupRole(savedGroupRole);
    }

    @Override
    public void deleteGroupRole(String groupRoleId) {
        // TODO: 31/10/2023 Implement deleting with key object
    }
}
