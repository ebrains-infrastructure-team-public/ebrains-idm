package eu.ebrains.idm.service;

import eu.ebrains.idm.model.dto.*;
import eu.ebrains.idm.rest.team.CreateTeamForm;
import eu.ebrains.idm.rest.team.CreateTeamResponse;
import eu.ebrains.idm.utils.enums.Role;

import java.util.List;

public interface TeamService {
    List<Team> getAllTeams();
    Team getTeamById(String teamId);
    Team getTeamByName(String teamName);
    CreateTeamResponse createTeam(CreateTeamForm form);
    Team updateTeam(String teamId, Team team);
    void deleteTeam(String teamName);
    void deleteUserFromTeam(String teamName, String username, String role);
    void deleteGroupFromTeam(String teamName, String groupName, String role);
    Membership getTeamMembershipByRole(String teamId, Role role);
    void addUserToTeam(String teamName, String role, String username);
    void addGroupToTeam(String teamName, String role, String groupName);
    List<CollabUser> getTeamMembershipUsersByRole(String teamName, String role);
    List<Group> getTeamMembershipGroupsByRole(String teamName, String role);
    List<Unit> getTeamMembershipUnitsByRole(String teamName, String role);

}