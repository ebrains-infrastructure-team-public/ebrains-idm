package eu.ebrains.idm.service;

import eu.ebrains.idm.model.dto.TeamMembershipGroup;
import eu.ebrains.idm.utils.enums.Role;

import java.util.List;

public interface TeamMembershipGroupService {
    List<TeamMembershipGroup> getAllTeamMembershipGroups();
    TeamMembershipGroup getTeamMembershipGroupById(String teamMembershipGroupId);
    TeamMembershipGroup createTeamMembershipGroup(TeamMembershipGroup teamMembershipGroup);
    TeamMembershipGroup updateTeamMembershipGroup(String teamMembershipGroupId, TeamMembershipGroup teamMembershipGroup);
    void deleteTeamMembershipGroup(String teamMembershipGroupId);
    TeamMembershipGroup getTeamMembershipGroupByTeamNameAndGroupId(String teamName, String groupId);

    List<TeamMembershipGroup> getTeamMembershipGroupByRole(String teamId, Role role);
}