package eu.ebrains.idm.service.impl;

import eu.ebrains.idm.mapper.TeamMembershipGroupMapper;
import eu.ebrains.idm.mapper.TeamMembershipGroupMapperImpl;
import eu.ebrains.idm.model.dto.TeamMembershipGroup;
import eu.ebrains.idm.model.normalized.TeamMembershipGroupEntity;
import eu.ebrains.idm.repository.TeamMembershipGroupRepository;
import eu.ebrains.idm.service.TeamMembershipGroupService;
import eu.ebrains.idm.utils.enums.Role;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class TeamMembershipGroupServiceImpl implements TeamMembershipGroupService {

    @Autowired
    private TeamMembershipGroupRepository teamMembershipGroupRepository;
    private TeamMembershipGroupMapper teamMembershipGroupMapper = new TeamMembershipGroupMapperImpl();

    @Override
    public List<TeamMembershipGroup> getAllTeamMembershipGroups() {
        List<TeamMembershipGroupEntity> teamMembershipGroups = teamMembershipGroupRepository.findAll();
        return teamMembershipGroupMapper.teamMembershipGroupEntitiesToTeamMembershipGroups(teamMembershipGroups);
    }

    @Override
    public TeamMembershipGroup getTeamMembershipGroupById(String teamMembershipGroupId) {
        Optional<TeamMembershipGroupEntity> teamMembershipGroup = teamMembershipGroupRepository.findById(teamMembershipGroupId);
        return teamMembershipGroup.map(teamMembershipGroupMapper::teamMembershipGroupEntityToTeamMembershipGroup).orElse(null);
    }

    @Override
    public TeamMembershipGroup createTeamMembershipGroup(TeamMembershipGroup teamMembershipGroup) {
        TeamMembershipGroupEntity teamMembershipGroupToSave = teamMembershipGroupMapper.teamMembershipGroupToTeamMembershipGroupEntity(teamMembershipGroup);
        TeamMembershipGroupEntity savedTeamMembershipGroup = teamMembershipGroupRepository.save(teamMembershipGroupToSave);
        return teamMembershipGroupMapper.teamMembershipGroupEntityToTeamMembershipGroup(savedTeamMembershipGroup);
    }

    @Override
    public TeamMembershipGroup updateTeamMembershipGroup(String teamMembershipGroupId, TeamMembershipGroup teamMembershipGroup) {
        return null;
    }

    @Override
    public void deleteTeamMembershipGroup(String teamMembershipGroupId) {
        teamMembershipGroupRepository.deleteById(teamMembershipGroupId);
    }

    @Override
    public TeamMembershipGroup getTeamMembershipGroupByTeamNameAndGroupId(String teamName, String groupId) {
        return teamMembershipGroupMapper.teamMembershipGroupEntityToTeamMembershipGroup(
                teamMembershipGroupRepository.findFirstByTeamNameIgnoreCaseAndGroupId(teamName, groupId).get()
        );
    }

    @Override
    public List<TeamMembershipGroup> getTeamMembershipGroupByRole(String teamId, Role role) {
        List<TeamMembershipGroupEntity> teamUsers = teamMembershipGroupRepository.findAllByTeamNameAndTeamRoleRole(teamId,role);
        return teamMembershipGroupMapper.teamMembershipGroupEntitiesToTeamMembershipGroups(teamUsers);
    }
}
