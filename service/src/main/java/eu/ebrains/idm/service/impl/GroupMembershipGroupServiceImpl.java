package eu.ebrains.idm.service.impl;

import eu.ebrains.idm.mapper.GroupMembershipGroupMapper;
import eu.ebrains.idm.mapper.GroupMembershipGroupMapperImpl;
import eu.ebrains.idm.model.dto.GroupMembershipGroup;
import eu.ebrains.idm.model.dto.TeamMembershipGroup;
import eu.ebrains.idm.model.dto.TeamMembershipUser;
import eu.ebrains.idm.model.normalized.GroupMembershipGroupEntity;
import eu.ebrains.idm.model.normalized.TeamMembershipGroupEntity;
import eu.ebrains.idm.repository.GroupMembershipGroupRepository;
import eu.ebrains.idm.service.GroupMembershipGroupService;
import eu.ebrains.idm.utils.enums.Role;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class GroupMembershipGroupServiceImpl implements GroupMembershipGroupService {

    @Autowired
    private GroupMembershipGroupRepository groupMembershipGroupRepository;
    private GroupMembershipGroupMapper groupMembershipGroupMapper = new GroupMembershipGroupMapperImpl();

    @Override
    public List<GroupMembershipGroup> getAllGroupMembershipGroups() {
        List<GroupMembershipGroupEntity> groupMembershipGroups = groupMembershipGroupRepository.findAll();
        return groupMembershipGroupMapper.groupMembershipGroupEntitiesToGroupMembershipGroups(groupMembershipGroups);
    }

    @Override
    public GroupMembershipGroup getGroupMembershipGroupById(String groupMembershipGroupId) {
        // TODO: 31/10/2023 Implement deleting with key object
        return null;
    }

    @Override
    public GroupMembershipGroup createGroupMembershipGroup(GroupMembershipGroup groupMembershipGroup) {
        GroupMembershipGroupEntity goupMembershipGroup = groupMembershipGroupMapper.groupMembershipGroupToGroupMembershipGroupEntity(groupMembershipGroup);
        GroupMembershipGroupEntity savedGoupMembershipGroup = groupMembershipGroupRepository.save(goupMembershipGroup);
        return groupMembershipGroupMapper.groupMembershipGroupEntityToGroupMembershipGroup(savedGoupMembershipGroup);
    }

    @Override
    public GroupMembershipGroup updateGroupMembershipGroup(String groupMembershipGroupId, GroupMembershipGroup groupMembershipGroup) {
        return null;
    }

    @Override
    public void deleteGroupMembershipGroup(String groupMembershipGroupId) {
        // TODO: 31/10/2023 Implement deleting with key object
    }

    @Override
    public List<GroupMembershipGroup> getGroupMembershipGroupByRole(String groupName, Role role) {
        List<GroupMembershipGroupEntity> groupsMembership = groupMembershipGroupRepository.findAllByParentGroupNameAndGroupRoleRole(groupName,role);
        return groupMembershipGroupMapper.groupMembershipGroupEntitiesToGroupMembershipGroups(groupsMembership);
    }

}
