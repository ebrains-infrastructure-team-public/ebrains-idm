package eu.ebrains.idm.service.impl;

import eu.ebrains.idm.mapper.TeamMembershipUserMapper;
import eu.ebrains.idm.mapper.TeamMembershipUserMapperImpl;
import eu.ebrains.idm.model.dto.TeamMembershipUser;
import eu.ebrains.idm.model.normalized.TeamMembershipUserEntity;
import eu.ebrains.idm.repository.TeamMembershipUserRepository;
import eu.ebrains.idm.service.TeamMembershipUserService;
import eu.ebrains.idm.utils.enums.Role;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class TeamMembershipUserServiceImpl implements TeamMembershipUserService {

    @Autowired
    private TeamMembershipUserRepository teamMembershipUserRepository;
    private TeamMembershipUserMapper teamMembershipUserMapper = new TeamMembershipUserMapperImpl();

    @Override
    public List<TeamMembershipUser> getAllTeamMembershipUsers() {
        List<TeamMembershipUserEntity> TeamMembershipUsers = teamMembershipUserRepository.findAll();
        return teamMembershipUserMapper.teamMembershipUserEntitiesToTeamMembershipUsers(TeamMembershipUsers);
    }

    @Override
    public TeamMembershipUser getTeamMembershipUserById(String TeamMembershipUserId) {
        Optional<TeamMembershipUserEntity> TeamMembershipUser = teamMembershipUserRepository.findById(TeamMembershipUserId);
        return TeamMembershipUser.map(teamMembershipUserMapper::teamMembershipUserEntityToTeamMembershipUser).orElse(null);
    }

    @Override
    public TeamMembershipUser createTeamMembershipUser(TeamMembershipUser teamMembershipUser) {
        TeamMembershipUserEntity teamMembershipUserToSave = teamMembershipUserMapper.teamMembershipUserToTeamMembershipUserEntity(teamMembershipUser);
        TeamMembershipUserEntity savedTeamMembershipUser = teamMembershipUserRepository.save(teamMembershipUserToSave);
        return teamMembershipUserMapper.teamMembershipUserEntityToTeamMembershipUser(savedTeamMembershipUser);
    }

    @Override
    public TeamMembershipUser updateTeamMembershipUser(String TeamMembershipUserId, TeamMembershipUser teamMembershipUser) {
        return null;
    }

    @Override
    public void deleteTeamMembershipUser(String TeamMembershipUserId) {
        teamMembershipUserRepository.deleteById(TeamMembershipUserId);
    }

    @Override
    public List<TeamMembershipUser> getTeamMembershipUserByRole(String teamName, Role role) {
        List<TeamMembershipUserEntity> teamUsers = teamMembershipUserRepository.findAllByTeamNameAndTeamRoleRole(teamName,role);
        return teamMembershipUserMapper.teamMembershipUserEntitiesToTeamMembershipUsers(teamUsers);
    }

    @Override
    public TeamMembershipUser getTeamMembershipUserByTeamIdAndUserId(String teamId, String userId) {
        return teamMembershipUserMapper.teamMembershipUserEntityToTeamMembershipUser(
                teamMembershipUserRepository.findFirstByTeamIdAndUserId(teamId, userId).get()
        );
    }

    @Override
    public TeamMembershipUser getTeamMembershipUserByTeamNameAndUserUsername(String teamName, String username) {
        return teamMembershipUserMapper.teamMembershipUserEntityToTeamMembershipUser(
                teamMembershipUserRepository.findFirstByTeamNameAndUserUsername(teamName, username).get()
        );
    }
}
