package eu.ebrains.idm.service.impl;

import eu.ebrains.idm.mapper.TeamRoleMapper;
import eu.ebrains.idm.mapper.TeamRoleMapperImpl;
import eu.ebrains.idm.model.dto.TeamRole;
import eu.ebrains.idm.model.normalized.TeamRoleEntity;
import eu.ebrains.idm.repository.TeamRoleRepository;
import eu.ebrains.idm.service.TeamRoleService;
import eu.ebrains.idm.utils.enums.Role;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class TeamRoleServiceImpl implements TeamRoleService {

    @Autowired
    private TeamRoleRepository teamRoleRepository;
    private TeamRoleMapper teamRoleMapper = new TeamRoleMapperImpl();

    @Override
    public List<TeamRole> getAllTeamRoles() {
        List<TeamRoleEntity> teamRoles = teamRoleRepository.findAll();
        return teamRoleMapper.teamRoleEntitiesToTeamRoles(teamRoles);
    }

    @Override
    public TeamRole getTeamRoleById(String teamRoleId) {
        Optional<TeamRoleEntity> teamRole = teamRoleRepository.findById(Long.valueOf(teamRoleId));
        return teamRole.map(teamRoleMapper::teamRoleEntityToTeamRole).orElse(null);
    }

    @Override
    public TeamRole getTeamRoleByName(Role role) {
        TeamRoleEntity teamRole = teamRoleRepository.findFirstByRole(role);
        return teamRoleMapper.teamRoleEntityToTeamRole(teamRole);
    }

    @Override
    public TeamRole createTeamRole(TeamRole teamRole) {
        TeamRoleEntity teamRoleToSave = teamRoleMapper.teamRoleToTeamRoleEntity(teamRole);
        TeamRoleEntity savedTeamRole = teamRoleRepository.save(teamRoleToSave);
        return teamRoleMapper.teamRoleEntityToTeamRole(savedTeamRole);
    }

    @Override
    public TeamRole updateTeamRole(String teamRoleId, TeamRole teamRole) {
        return null;
    }

    @Override
    public void deleteTeamRole(String teamRoleId) {
        teamRoleRepository.deleteById(Long.valueOf(teamRoleId));
    }
}
