package eu.ebrains.idm.service;

import eu.ebrains.idm.model.dto.CollabUser;
import eu.ebrains.idm.model.dto.Membership;
import eu.ebrains.idm.rest.group.UnitDetail;

import java.util.List;

public interface UnitService {

    UnitDetail getUnitsWithFlatListMembersByPath(String unitName);
    UnitDetail getUnitsWithMembersByPath(String unitName);
    Membership getMembershipAdministratorForUnit(String unitName);
    List<CollabUser> getUsersAdminForUnit();
}