package eu.ebrains.idm.service.impl;

import eu.ebrains.idm.mapper.GroupMapper;
import eu.ebrains.idm.model.dto.CollabUser;
import eu.ebrains.idm.model.dto.Membership;
import eu.ebrains.idm.model.dto.Unit;
import eu.ebrains.idm.repository.GroupRepository;
import eu.ebrains.idm.rest.group.UnitDetail;
import eu.ebrains.idm.service.UnitService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Service
public class UnitServiceImpl implements UnitService {

    @Autowired
    private GroupRepository groupRepository;
    @Autowired
    private GroupMapper groupMapper;

    @Override
    public UnitDetail getUnitsWithFlatListMembersByPath(String unitName) {
        Unit unit = groupMapper.groupToUnit(groupMapper.groupEntityToGroup(groupRepository.findByNameIgnoreCase(unitName).get()));
        List<CollabUser> members = new ArrayList<CollabUser>();
//        userRepository.getUsersFlatListByUnit(unit, members);

        Set<CollabUser> uniqueMembers = new HashSet<CollabUser>(members);
        members.clear();
        members.addAll(uniqueMembers);

        return new UnitDetail(unit, members);
    }

    @Override
    public UnitDetail getUnitsWithMembersByPath(String unitName) {
        return null;
    }

    @Override
    public Membership getMembershipAdministratorForUnit(String unitName) {
        Membership membership = new Membership();
        return null;
    }

    @Override
    public List<CollabUser> getUsersAdminForUnit() {
        return null;
    }
}
