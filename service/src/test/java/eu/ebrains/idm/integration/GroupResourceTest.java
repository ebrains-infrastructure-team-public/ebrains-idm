package eu.ebrains.idm.integration;

import eu.ebrains.idm.IDMApplication;
import eu.ebrains.idm.model.dto.Group;
import eu.ebrains.idm.model.dto.Membership;
import eu.ebrains.idm.model.normalized.*;
import eu.ebrains.idm.repository.*;
import eu.ebrains.idm.rest.group.GroupResource;
import eu.ebrains.idm.rest.team.CreateTeamForm;
import eu.ebrains.idm.rest.team.TeamResource;
import eu.ebrains.idm.service.GroupService;
import eu.ebrains.idm.service.TeamService;
import eu.ebrains.idm.utils.enums.GroupType;
import eu.ebrains.idm.utils.enums.Role;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@SpringBootTest(classes= GroupResource.class)
@AutoConfigureMockMvc
@ContextConfiguration(classes = IDMApplication.class)
@Transactional
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
public class GroupResourceTest extends AbstractResourceTest {

    private static final Logger logger = LoggerFactory.getLogger(GroupResourceTest.class);

    @Autowired
    private GroupService groupService;
    @Autowired
    private TeamRepository teamRepository;
    @Autowired
    private GroupRoleRepository groupRoleRepository;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private GroupRepository groupRepository;
    @Autowired
    private GroupMembershipUserRepository groupMembershipUserRepository;
    @Autowired
    private GroupMembershipGroupRepository groupMembershipGroupRepository;

    private static final String GROUP_NAME = "integration-test-group";
    private static final String USER_PAOLO = "paolo";
    private static final String USER_TOTO = "toto";
    private static final String USER_MARIE = "marie";
    private String GROUP_1 = "group1";
    private String GROUP_2 = "group2";

    @BeforeEach
    @Transactional
    public void before() {
        GroupEntity group = new GroupEntity(GROUP_NAME, "Integration Test Group","Team for integration test", GroupType.GROUP);
        UserEntity user = new UserEntity(USER_PAOLO,"john@exemple.com",false);
        UserEntity user2 = new UserEntity(USER_TOTO,"toto@exemple.com",false);
        UserEntity user3 = new UserEntity(USER_MARIE,"marie@exemple.com",false);

        GroupRoleEntity groupRole = new GroupRoleEntity(Role.ADMINISTRATOR);

        GroupEntity group1 = new GroupEntity(GROUP_1,"Group 1","Le groupe 1 de test", GroupType.GROUP);
        GroupEntity group2 = new GroupEntity(GROUP_2,"Group 2","Le groupe 2 de test", GroupType.GROUP);

        user = userRepository.saveAndFlush(user);
        user2 = userRepository.saveAndFlush(user2);
        user3 = userRepository.saveAndFlush(user3);

        group1 = groupRepository.saveAndFlush(group1);
        group2 = groupRepository.saveAndFlush(group2);

        group = groupRepository.saveAndFlush(group);

        GroupRoleEntity groupRoleEntity = groupRoleRepository.saveAndFlush(groupRole);

        GroupMembershipUserEntity groupMembershipUser = new GroupMembershipUserEntity(group,user,groupRoleEntity);
        GroupMembershipUserEntity groupMembershipUser2 = new GroupMembershipUserEntity(group,user2,groupRoleEntity);
        groupMembershipUserRepository.saveAndFlush(groupMembershipUser);
        groupMembershipUserRepository.saveAndFlush(groupMembershipUser2);

        GroupMembershipGroupEntity teamMembershipGroup = new GroupMembershipGroupEntity(group,group1,groupRoleEntity);
        GroupMembershipGroupEntity teamMembershipGroup2 = new GroupMembershipGroupEntity(group,group2,groupRoleEntity);

        groupMembershipGroupRepository.saveAndFlush(teamMembershipGroup);
        groupMembershipGroupRepository.saveAndFlush(teamMembershipGroup2);

    }
    @AfterEach
    public void after() {
        logger.info("Clean up tests data");
    }

    @Test
    @WithMockUser(username = USER_PAOLO, authorities = {"SCOPE_openid"})
    public void createGroup() throws Exception {
        String groupName = "test-creation-group";
        Group groupForm = new Group();
        groupForm.setName(groupName);
        groupForm.setTitle("Test Creation Group");
        groupForm.setDescription("Integration test test");
        groupForm.setType(GroupType.GROUP);

        groupService.createGroup(groupForm);

        Optional<GroupEntity> groupCreated = groupRepository.findByNameIgnoreCase(groupName);
        assertTrue(groupCreated.isPresent());
    }
    @Test
    public void getGroupMembershipAdministrators() throws Exception {

        //DEBUG
        Membership membership = groupService.getGroupMembershipByRole(GROUP_NAME,Role.ADMINISTRATOR);

        //TEST
        String requestUrl = "/groups/"+GROUP_NAME+"/"+Role.ADMINISTRATOR.getKey();

        MvcResult result = mockMvc.perform(get(requestUrl))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andReturn();

        MockHttpServletResponse mResponse = result.getResponse();
        String jsonResponse = mResponse.getContentAsString();

        Membership groupAdmins = jsonToDTO(jsonResponse,Membership.class);

        assertEquals(2, groupAdmins.getUsers().size());
        assertEquals(2, groupAdmins.getGroups().size());
        assertTrue(groupAdmins.getUsers().stream().anyMatch(user -> USER_PAOLO.equals(user.getUsername())));
        assertTrue(groupAdmins.getUsers().stream().anyMatch(user -> USER_TOTO.equals(user.getUsername())));
        assertTrue(groupAdmins.getGroups().stream().anyMatch(group -> GROUP_1.equals(group.getName())));
        assertTrue(groupAdmins.getGroups().stream().anyMatch(group -> GROUP_2.equals(group.getName())));
    }

    @Test
    public void addUserToGroup() throws Exception {
        groupService.addUserToGroup(GROUP_NAME,Role.ADMINISTRATOR.getKey(),USER_MARIE);

        List<GroupMembershipUserEntity> users = groupMembershipUserRepository.findAllByGroupNameAndGroupRoleRole(GROUP_NAME,Role.ADMINISTRATOR);
        Optional<GroupMembershipUserEntity> checkUserMarie = groupMembershipUserRepository.findFirstByGroupNameAndUserUsername(GROUP_NAME,USER_MARIE);

        assertEquals(3,users.size());
        assertTrue(checkUserMarie.isPresent());
    }
}
