package eu.ebrains.idm.integration;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import eu.ebrains.idm.IDMApplication;
import eu.ebrains.idm.exception.ErrorMessage;
import eu.ebrains.idm.model.dto.Membership;
import eu.ebrains.idm.model.normalized.*;
import eu.ebrains.idm.repository.*;
import eu.ebrains.idm.rest.team.CreateTeamForm;
import eu.ebrains.idm.rest.team.CreateTeamResponse;
import eu.ebrains.idm.rest.team.TeamResource;
import eu.ebrains.idm.service.TeamService;
import eu.ebrains.idm.utils.enums.GroupType;
import eu.ebrains.idm.utils.enums.Role;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@SpringBootTest(classes= TeamResource.class)
@AutoConfigureMockMvc
@ContextConfiguration(classes = IDMApplication.class)
@Transactional
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
public class TeamResourceTest extends AbstractResourceTest {

    private static final Logger logger = LoggerFactory.getLogger(TeamResourceTest.class);

    @Autowired
    private TeamService teamService;

    @Autowired
    private TeamRepository teamRepository;

    @Autowired
    private TeamRoleRepository teamRoleRepository;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private GroupRepository groupRepository;

    @Autowired
    private TeamMembershipUserRepository teamMembershipUserRepository;

    @Autowired
    private TeamMembershipGroupRepository teamMembershipGroupRepository;

    private static final String TEAM_NAME = "integration-test-team";
    private static final String USER_PAOLO = "paolo";
    private static final String USER_TOTO = "toto";
    private static final String USER_MARIE = "marie";
    private String GROUP_1 = "group1";
    private String GROUP_2 = "group2";

    @BeforeEach
    @Transactional
    public void before() {
        TeamEntity team = new TeamEntity(TEAM_NAME, "Team for integration test");
        UserEntity user = new UserEntity(USER_PAOLO,"john@exemple.com",false);
        UserEntity user2 = new UserEntity(USER_TOTO,"toto@exemple.com",false);
        UserEntity user3 = new UserEntity(USER_MARIE,"marie@exemple.com",false);

        TeamRoleEntity teamRole = new TeamRoleEntity(Role.ADMINISTRATOR);

        GroupEntity group1 = new GroupEntity(GROUP_1,"Group 1","Le groupe 1 de test", GroupType.GROUP);
        GroupEntity group2 = new GroupEntity(GROUP_2,"Group 2","Le groupe 2 de test", GroupType.GROUP);

        user = userRepository.saveAndFlush(user);
        user2 = userRepository.saveAndFlush(user2);
        user3 = userRepository.saveAndFlush(user3);

        group1 = groupRepository.saveAndFlush(group1);
        group2 = groupRepository.saveAndFlush(group2);

        team = teamRepository.saveAndFlush(team);

        TeamRoleEntity teamRoleEntity = teamRoleRepository.saveAndFlush(teamRole);

        TeamMembershipUserEntity teamMembershipUser = new TeamMembershipUserEntity(user, team, teamRoleEntity);
        TeamMembershipUserEntity teamMembershipUser2 = new TeamMembershipUserEntity(user2, team, teamRoleEntity);
        teamMembershipUserRepository.saveAndFlush(teamMembershipUser);
        teamMembershipUserRepository.saveAndFlush(teamMembershipUser2);

        TeamMembershipGroupEntity teamMembershipGroup = new TeamMembershipGroupEntity(group1,team,teamRoleEntity);
        TeamMembershipGroupEntity teamMembershipGroup2 = new TeamMembershipGroupEntity(group2,team,teamRoleEntity);

        teamMembershipGroupRepository.saveAndFlush(teamMembershipGroup);
        teamMembershipGroupRepository.saveAndFlush(teamMembershipGroup2);

    }
    @AfterEach
    public void after() {
        logger.info("Clean up tests data");
    }

    @Test
    @WithMockUser(username = USER_PAOLO, authorities = {"SCOPE_openid"})
    public void createTeamThenOK() throws Exception {
        String teamName = "test-creation-team";
        CreateTeamForm teamForm = new CreateTeamForm();
        teamForm.setName(teamName);
        teamForm.setDescription("Integration test test");

        ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
        String json = ow.writeValueAsString(teamForm);

        MvcResult result = mockMvc.perform(post("/teams").content(json).contentType(contentType))
                .andExpect(status().isOk())
                .andReturn();

        MockHttpServletResponse mResponse = result.getResponse();
        String jsonResponse = mResponse.getContentAsString();

        CreateTeamResponse response = jsonToDTO(jsonResponse, CreateTeamResponse.class);

        assertTrue(response.getName().equals("test-creation-team"));
        assertTrue(response.getDescription().equals("Integration test test"));
    }

    @Test
    @WithMockUser(username = USER_PAOLO, authorities = {"SCOPE_openid"})
    public void createTeamThenReturnInvalidParameterException() throws Exception {
        // empty teamName
        CreateTeamForm teamFormWithEmptyName = new CreateTeamForm();
        teamFormWithEmptyName.setName("");
        teamFormWithEmptyName.setDescription("Integration test test");

        ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
        String json = ow.writeValueAsString(teamFormWithEmptyName);

        MvcResult result = mockMvc.perform(post("/teams").content(json).contentType(contentType))
                .andExpect(status().isBadRequest())
                .andReturn();

        MockHttpServletResponse mResponse = result.getResponse();
        String jsonResponse = mResponse.getContentAsString();

        ErrorMessage response = jsonToDTO(jsonResponse, ErrorMessage.class);

        assertEquals(response.getStatusCode(), 400);
        assertEquals(response.getMessage(), ErrorMessage.TEAM_NAME_EMPTY_ERROR_MSG);

        // empty teamDescription
        CreateTeamForm teamFormwithEmptyDescription = new CreateTeamForm();
        teamFormwithEmptyDescription.setName("team-test");
        teamFormwithEmptyDescription.setDescription("");

        json = ow.writeValueAsString(teamFormwithEmptyDescription);

        result = mockMvc.perform(post("/teams").content(json).contentType(contentType))
                .andExpect(status().isBadRequest())
                .andReturn();

        mResponse = result.getResponse();
        jsonResponse = mResponse.getContentAsString();

        response = jsonToDTO(jsonResponse, ErrorMessage.class);

        assertEquals(response.getStatusCode(), 400);
        assertEquals(response.getMessage(), ErrorMessage.TEAM_DESCRIPTION_EMPTY_ERROR_MSG);

        // too short teamName
        CreateTeamForm teamFormwithLongName = new CreateTeamForm();
        teamFormwithLongName.setName("tea");
        teamFormwithLongName.setDescription("Team description");

        json = ow.writeValueAsString(teamFormwithLongName);

        result = mockMvc.perform(post("/teams").content(json).contentType(contentType))
                .andExpect(status().isBadRequest())
                .andReturn();

        mResponse = result.getResponse();
        jsonResponse = mResponse.getContentAsString();

        response = jsonToDTO(jsonResponse, ErrorMessage.class);

        assertEquals(response.getStatusCode(), 400);
        assertEquals(response.getMessage(), ErrorMessage.TEAM_NAME_SHORT_OR_LONG_ERROR_MSG);

        // teamName with special chars
        CreateTeamForm teamFormWithNameWithSpecialChars = new CreateTeamForm();
        teamFormWithNameWithSpecialChars.setName("team-test-@");
        teamFormWithNameWithSpecialChars.setDescription("Team description");

        json = ow.writeValueAsString(teamFormWithNameWithSpecialChars);

        result = mockMvc.perform(post("/teams").content(json).contentType(contentType))
                .andExpect(status().isBadRequest())
                .andReturn();

        mResponse = result.getResponse();
        jsonResponse = mResponse.getContentAsString();

        response = jsonToDTO(jsonResponse, ErrorMessage.class);

        assertEquals(response.getStatusCode(), 400);
        assertEquals(response.getMessage(), ErrorMessage.TEAM_NAME_WITH_SPECIAL_CHARS_ERROR_MSG);
    }

    @Test
    public void createTeamThenThrowAuthorizationException() throws Exception {
        String teamName = "test-creation-team";
        CreateTeamForm teamForm = new CreateTeamForm();
        teamForm.setName(teamName);
        teamForm.setDescription("Integration test test");

        ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
        String json = ow.writeValueAsString(teamForm);

        MvcResult result = mockMvc.perform(post("/teams").content(json).contentType(contentType))
                .andExpect(status().isForbidden())
                .andReturn();

        assertEquals(result.getResponse().getStatus(), 403);
    }

    @Test
    @WithMockUser(username = USER_PAOLO, authorities = {"SCOPE_openid"})
    public void createTeamThenReturnAlreadyExistException() throws Exception {
        CreateTeamForm form = new CreateTeamForm();
        form.setName("team-test-already-existed");
        form.setDescription("team description");

        ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
        String json = ow.writeValueAsString(form);

        MvcResult result1 = mockMvc.perform(post("/teams").content(json).contentType(contentType))
                .andExpect(status().isOk())
                .andReturn();

        MvcResult result2 = mockMvc.perform(post("/teams").content(json).contentType(contentType))
                .andExpect(status().isConflict())
                .andReturn();

        MockHttpServletResponse mResponse = result2.getResponse();
        String jsonResponse = mResponse.getContentAsString();

        ErrorMessage response = jsonToDTO(jsonResponse, ErrorMessage.class);

        assertEquals(response.getStatusCode(), 409);
        assertEquals(response.getMessage(), ErrorMessage.TEAM_ALREADY_EXIST_ERROR_MSG);
    }

    @Test
    @WithMockUser(username = USER_PAOLO, authorities = {"SCOPE_openid"})
    public void createTeam() throws Exception {
        String teamName = "test-creation-team";
        CreateTeamForm teamForm = new CreateTeamForm();
        teamForm.setName(teamName);
        teamForm.setDescription("Integration test test");

        teamService.createTeam(teamForm);
        /*
        MvcResult result = mockMvc.perform(post("/teams").content(json(teamForm)).contentType(contentType))  // Remplacez "/your-endpoint" par l'URL de votre endpoint
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andReturn();

         */

        Optional<TeamEntity> teamEntity = teamRepository.findByNameIgnoreCase(teamName);

        assertTrue(teamEntity.isPresent());

        if(teamEntity.isPresent()) {
            TeamEntity team = teamEntity.get();
            assertNotNull(teamEntity.get().getDateCreation());
            assertEquals(USER_PAOLO,teamEntity.get().getUserCreation());
        }
    }

    @Test
    public void getTeamMembershipAdministrators() throws Exception {

        //DEBUG
        //Membership membership = teamService.getTeamMembershipByRole(TEAM_NAME,Role.ADMINISTRATOR);

        //TEST
        String requestUrl = "/teams/"+TEAM_NAME+"/"+Role.ADMINISTRATOR.getKey();

        MvcResult result = mockMvc.perform(get(requestUrl))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andReturn();

        MockHttpServletResponse mResponse = result.getResponse();
        String jsonResponse = mResponse.getContentAsString();

        Membership teamAdmins = jsonToDTO(jsonResponse,Membership.class);

        assertEquals(2, teamAdmins.getUsers().size());
        assertEquals(2, teamAdmins.getGroups().size());
        assertTrue(teamAdmins.getUsers().stream().anyMatch(user -> USER_PAOLO.equals(user.getUsername())));
        assertTrue(teamAdmins.getUsers().stream().anyMatch(user -> USER_TOTO.equals(user.getUsername())));
        assertTrue(teamAdmins.getGroups().stream().anyMatch(group -> GROUP_1.equals(group.getName())));
        assertTrue(teamAdmins.getGroups().stream().anyMatch(group -> GROUP_2.equals(group.getName())));
    }

    @Test
    public void addUserToTeam() throws Exception {

        String requestUrl = "/teams/" + TEAM_NAME + "/" + Role.ADMINISTRATOR.getKey() +  "/users/" + USER_MARIE;

        //teamService.addUserToTeam(TEAM_NAME,Role.ADMINISTRATOR.getKey(), USER_MARIE);

        mockMvc.perform(put(requestUrl)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNoContent());


        requestUrl = "/teams/"+TEAM_NAME+"/"+Role.ADMINISTRATOR.getKey();

        MvcResult result = mockMvc.perform(get(requestUrl))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andReturn();


        MockHttpServletResponse mResponse = result.getResponse();
        String jsonResponse = mResponse.getContentAsString();

        Membership teamAdmins = jsonToDTO(jsonResponse,Membership.class);

        assertEquals(3, teamAdmins.getUsers().size());
        assertEquals(2, teamAdmins.getGroups().size());
        assertTrue(teamAdmins.getUsers().stream().anyMatch(user -> USER_MARIE.equals(user.getUsername())));
    }
}
